import re
from odoo import http
from odoo.http import request
from odoo.addons.website_hr_recruitment.controllers.main import WebsiteHrRecruitment
from odoo.exceptions import UserError
from odoo.addons.auth_signup.models.res_partner import SignupError
from datetime import datetime
import mimetypes
import base64
import logging
Logger = logging.getLogger(__name__)
from odoo.addons.http_routing.models.ir_http import unslug_url

class Pages(http.Controller):
    @http.route(['/', '/web/home'], auth='public',type='http',website=True)
    def web_home(self, **kw):
        news = request.env['web_tai_news.article'].sudo().search([('rubric_id','!=',False),('state','=','posted')],limit=3,order='create_date desc')
        jobs = request.env['hr.job'].sudo().search([('is_published', '=', True)],order="is_published desc",limit=6)
        return request.render('theme_tai.tai_home_page', {'jobs':jobs,'news':news})

    @http.route('/web/talent', auth='public',type='http',website=True)
    def web_talent(self, **kw):
        persons = request.env['res.partner'].search([('is_company', '=', False),('display_testimonial', '=', True),('user_type', '=', 'jta')]) 
        return request.render('theme_tai.tai_talent_page', {'persons':persons})

    @http.route('/web/enterprise', auth='public',type='http',website=True)
    def web_entreprise(self, **kw):
        representatives = request.env['res.partner'].search([('is_company', '=', False),('display_testimonial', '=', True),('user_type', '=', 'representative')])
        return request.render('theme_tai.tai_enterprise_page', {'representatives':representatives})

    @http.route('/web/mission', auth='public',type='http',website=True)
    def web_misson(self, **kw):
        jobs = request.env['hr.job'].sudo().search([('is_published', '=', True)],order="is_published desc")
        countries = request.env['res.country'].search([('is_umoa', '=', True)])
        activity_areas = request.env['activity.area'].search([])
        company = request.env['res.partner'].search([('is_company', '=', True)])       
        return request.render('theme_tai.tai_mission_page', {'jobs':jobs, 'countries': countries, 'activity_areas': activity_areas, 'company': company})

    @http.route('/web/missions/all', auth='public',type='http',website=True)
    def web_destination(self, **kw):
        jobs = request.env['hr.job'].sudo().search([('is_published', '=', True)],order="is_published desc")
        countries = request.env['res.country'].search([('is_umoa', '=', True)])
        activity_areas = request.env['activity.area'].search([])
        company = request.env['res.partner'].search([('is_company', '=', True)])       
        return request.render('theme_tai.tai_all_mission_page', {'jobs':jobs, 'countries': countries, 'activity_areas': activity_areas, 'company': company})
    
    @http.route('/web/formation', auth='public',type='http',website=True)
    def web_formation(self, **kw):
        return request.render('theme_tai.tai_formation_page', {})
    
    @http.route('/tai/job/apply', auth='public',type='http',website=True)
    def submit_job(self, **kw): 
     
        Attachment = request.env['ir.attachment']
        binary_field = kw.get('binary_field')
        job_id = kw.get('job_id')
        job = request.env['hr.job'].sudo().search([('id','=',job_id)])
        motivation_letter = kw.get('motivation_letter')
        motivation_letter_id = Attachment.sudo().create({
            'name': 'Lettre_motivation_%s' % request.env.user.partner_id.name,
            'res_name': motivation_letter.name,
            'type': 'binary',
            'res_model': 'hr.applicant',
            'res_id': job_id,
            'datas': base64.encodebytes(motivation_letter.read())
        })
        attachment_id = Attachment.sudo().create({
            'name': 'CV_%s' % request.env.user.partner_id.name,
            'res_name': request.env.user.partner_id.curriculum_vitae.name or binary_field.name,
            'type': 'binary',
            'res_model': 'hr.applicant',
            'res_id': job_id,
            'datas': request.env.user.partner_id.curriculum_vitae.datas or base64.encodebytes(binary_field.read())
        })
        kw.update({'attachment_ids': [(4,attachment_id.id),(4, motivation_letter_id.id)], 'partner_id' : request.env.user.partner_id.id})
        kw.pop('binary_field', '')
        kw.pop('motivation_letter', '')
        job.write({'applied_user_ids': [(3, request.env.uid)]})
        job.write({'applied_user_ids': [(4, request.env.uid)]})
        request.env['hr.applicant'].sudo().create(kw)

        return request.redirect('/job-thank-you')
    
    @http.route('/web/a-propos', auth='public',type='http',website=True)
    def web_aboutus(self, **kw):
        return request.render('theme_tai.tai_about_us_page', {})
    
    @http.route('/contactus', auth='public',type='http',website=True)
    def tai_contactus(self, **kw):
        users = request.env['res.partner'].sudo().search([('unit_manager', '=', True)],order="country_id asc") 
        return request.render('theme_tai.tai_contactus_page', {'users':users})
    
    @http.route('''/web/form/contactus/<model("res.partner"):user>''', type='http', auth="public", website=True, sitemap=True)
    def contactus_form_tai(self, user, **kw):
        return request.render('theme_tai.tai_contactus_form',{'user' : user})
    
    @http.route('/web/FAQ', auth='public',type='http',website=True)
    def web_faq(self, **kw):
        questions = request.env['web_tai_faq.question'].sudo().search([('state','=','posted')],order="faq_sequence asc")
        rubrics = request.env['web_tai_faq.rubric'].sudo().search([('type_rubric','=','faq')])
        return request.render('theme_tai.tai_faq_page', {'questions':questions ,'rubrics':rubrics})

    @http.route('/web/actualite', auth='public',type='http',website=True)
    def web_actualite(self, **kw):
        news = request.env['web_tai_news.article'].sudo().search([('rubric_id','!=',False),('state','=','posted')],limit=9,order='create_date desc')
        rubrics = request.env['web_tai_faq.rubric'].sudo().search([('article_ids','!=',False),('article_ids.state','=','posted')],limit=3,order='name asc')
        return request.render('theme_tai.tai_actualite_page', {'news':news,'rubrics':rubrics})

    @http.route('''/web/actualite_detail/<model("web_tai_news.article"):new>''', auth='public',type='http',website=True)
    def web_actualite_detail(self,new,**kw):
        return request.render('theme_tai.tai_actualite_detail_page', {'new':new})

class WebsiteHrRecruitmentInherit(WebsiteHrRecruitment):
    def sitemap_jobs(env, rule, qs):
        if not qs or qs.lower() in '/web/mission':
            yield {'loc': '/web/mission'} 

    @http.route('''/web/mission/detail/<string:job_id>''', type='http', auth="public", website=True, sitemap=True)
    def jobs_detail(self, job_id, **kwargs):
        Logger.error(unslug_url(job_id))
        job_id = unslug_url(job_id)
        job = request.env['hr.job'].sudo().search([('id','=',job_id)])
        # res = super(WebsiteHrRecruitmentInherit, self).jobs_detail(job, **kwargs)
        return request.render('theme_tai.tai_mission_detail',{'job':job})

   
    @http.route('''/web/mission/apply/<model("hr.job"):job>''', type='http', auth="user", website=True, sitemap=True)
    def jobs_apply(self, job, **kwargs):
        # c_vitae = request.env.user.partner_id.curriculum_vitae_attachment_ids
        # Logger.error(c_vitae.attachment_id.name)
        study_level = request.env['res.partner.school_curriculum'].sudo().search([('partner_id', '=', request.env.user.partner_id.id)],limit=1,order='create_date desc')
        error = {}
        default = {}

        cv = False
        if request.env.user.partner_id.curriculum_vitae:
            cv = '%s%s' % (request.env.user.partner_id.curriculum_vitae.name, mimetypes.guess_extension(request.env.user.partner_id.curriculum_vitae.mimetype or ''))
        
        if 'website_hr_recruitment_error' in request.session:
            error = request.session.pop('website_hr_recruitment_error')
            default = request.session.pop('website_hr_recruitment_default')
        return request.render("theme_tai.tai_job_apply", {
            'cv': cv,
            'study_level': study_level,
            'job': job,
            'error': error,
            'default': default,
        })
        
    @http.route('/web/save/job', auth='user',type='http',website=True)
    def save_job(self, **kw):    
        job_id =kw.get('job_id')  
        job = request.env['hr.job'].sudo().search([('id','=',job_id)])
        if request.env.user in job.favorite_user_ids:
            job.write({'favorite_user_ids': [(3, request.env.uid)]})    
        else:     
            job.write({'favorite_user_ids': [(4, request.env.uid)]})  
                 
        return request.redirect('/web/mission/detail/%s'%job_id)
    
    @http.route(['/json_call_fun'], type='json', auth="public", website=True)
    def some_url(self, **arg):
        temp_question = arg.get('question')
        return {"res_question":temp_question}

    @http.route('''/web/questions/all/<model("web_tai_faq.rubric"):rubric>''', type='http', auth="public", website=True, sitemap=True)
    def web_questions(self,rubric, **kwargs):
        questions = request.env['web_tai_faq.question'].sudo().search([('state','=','posted')],order="faq_sequence asc")
        return request.render('theme_tai.all_questions_page', {'questions':questions ,'rubric':rubric})
