from odoo import http
from odoo.http import request

class Talent(http.Controller):
    @http.route('/web/destination', auth='public',type='http',website=True)
    def web_destination(self, **kw):
        return request.render('theme_tai.tai_destination_page', {})
    
    @http.route('/web/destination/senegal', auth='public',type='http',website=True)
    def web_destination_sn(self, **kw):
        return request.render('theme_tai.tai_destination_sn', {})

    @http.route('/web/destination/cote_divoire', auth='public',type='http',website=True)
    def web_destination_detail_ci(self, **kw):
        return request.render('theme_tai.tai_destination_cote_divoire', {})
    
    @http.route('/web/destination/benin', auth='public',type='http',website=True)
    def web_destination_detail_benin(self, **kw):
        return request.render('theme_tai.tai_destination_benin', {})
    
    @http.route('/web/destination/burkina_faso', auth='public',type='http',website=True)
    def web_destination_detail_BF(self, **kw):
        return request.render('theme_tai.tai_destination_burkina_faso', {})
