odoo.define('theme_tai.job_apply', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    const _t = core._t;

    publicWidget.registry.JobApply = publicWidget.Widget.extend({
        selector: '.job_apply_c_vitae',
        read_events: {
            'change .ao_on_upload': '_onFileUploadChange'
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        start: async function () {
            var def = this._super.apply(this, arguments);
            return Promise.all([def]);
        },

        _onFileUploadChange: function (ev) {
            if (!ev.currentTarget.files.length) {
                return;
            }

            var reader = new window.FileReader();
            reader.readAsDataURL(ev.currentTarget.files[0]);

            var fileName = ev.currentTarget.files[0].name

            var self = this
            reader.onload = function (ev) {
                 self.$el.find('.js-fileName').html(fileName);
                 self.$el.find('.icon').removeClass('fa-upload');
                 self.$el.find('.icon').addClass('fa-check text-primary');
                 self.$el.find('#cvHelp').hide();
            };
        },

    });

    return publicWidget.registry.JobApply;

});
