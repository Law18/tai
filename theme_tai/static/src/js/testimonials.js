function resizeTestimonials(classname, selector) {
    if (document.querySelector("." + classname) && document.querySelector(selector)) {
        let listItem = document.querySelectorAll(selector);
        for (const item of listItem) {
            item.classList.remove("carousel-item");
            item.classList.remove("active");
        }

        /* **** */

        const jta_testimonials = document.querySelectorAll("." + classname);
        let max_height = 0;
        for (let testimonial of jta_testimonials) {
            testimonial.style.height = "fit-content";
        }
        for (let testimonial of jta_testimonials) {
            if (testimonial.clientHeight > max_height) {
                max_height = testimonial.clientHeight;
            }
        }
        for (let testimonial of jta_testimonials) {
            testimonial.style.height = max_height + "px";
        }

        /* **** */

        for (const item of listItem) {
            item.classList.add("carousel-item");
        }
        listItem[0].classList.add("active");

    }
}
function initCarouselItem(selector) {
    if (document.querySelector(selector)) {
        let listItem = document.querySelectorAll(selector);
        for (const item of listItem) {
            item.classList.add("carousel-item");
        }
        listItem[0].classList.add("active");
    }
}
function displayCarouselIndicators(id, selector, target) {
    if (document.getElementById(id) && document.querySelector(selector)) {
        let jtaCarouselIndicator = document.getElementById(id);
        let listItem = document.querySelectorAll(selector);
        let contentJtaIndicators = "";
        for (let i = 0; i < listItem.length; i++) {
            if (i === 0) {
                contentJtaIndicators = contentJtaIndicators + `<li data-target="${target}" data-slide-to="${i}" class="active"></li>`;
            }
            else {
                contentJtaIndicators = contentJtaIndicators + `<li data-target="${target}" data-slide-to="${i}"></li>`;
            }
        }
        jtaCarouselIndicator.innerHTML = contentJtaIndicators;
    }
}

resizeTestimonials("jta_testimonial", "#jta_testimonial_carousel_inner > div");
resizeTestimonials("enterprise_testimonial", "#enterprise_testimonial_carousel_inner > div");
displayCarouselIndicators("jta-carousel-indicator", "#jta_testimonial_carousel_inner > div", "#jta_testimonials_container_slider");
displayCarouselIndicators("enterprise-carousel-indicator", "#enterprise_testimonial_carousel_inner > div", "#enterprise_testimonials_container_slider");

window.addEventListener('resize', () => {
    resizeTestimonials("jta_testimonial", "#jta_testimonial_carousel_inner > div");
    resizeTestimonials("enterprise_testimonial", "#enterprise_testimonial_carousel_inner > div");
});