/* Select elements filtered */
const html_class = "mission_by6";
let list = document.querySelectorAll("." + html_class);
let selected_element = [];
for (let i = 0; i < list.length; i++) {
    selected_element[i] = true;
}
let selected_element_length = list.length;
/* Pagination */
const max_item = 6;
let page = 1;
function display_pagination_link() {
    if (page >= Math.ceil(selected_element_length / max_item)) {
        if (document.getElementById("pagination_suivant")) {
            document.getElementById("pagination_suivant").disabled = true;
        }
    }
    else {
        if (document.getElementById("pagination_suivant")) {
            document.getElementById("pagination_suivant").removeAttribute("disabled");
        }
    }


    if (page <= 1) {
        if (document.getElementById("pagination_precedent")) {
            document.getElementById("pagination_precedent").disabled = true;
        }
    }
    else {
        if (document.getElementById("pagination_precedent")) {
            document.getElementById("pagination_precedent").removeAttribute("disabled");
        }
    }
}

function suivant() {
    page++;
    update_element_length();
    display_pagination_link();
    display_pagination();
    display_element();
}

function precedent() {
    page--;
    update_element_length();
    display_pagination_link();
    display_pagination();
    display_element();
}

function display_pagination() {/* 
    selected_element_length = 0;
    for (let i = 0; i < list.length; i++)
        selected_element[i] ? selected_element_length++ : ""; */
    if (document.getElementById("pagination")) {
        document.getElementById("pagination").innerHTML = page + " / " + Math.ceil(selected_element_length / max_item);
    }
}

function update_element_length() {
    selected_element_length = 0
    for (let i = 0; i < list.length; i++)
        selected_element[i] ? selected_element_length++ : "";
}

function display_element() {
    let elements_to_display = [];
    for (let i = 0; i < list.length; i++) {
        if (selected_element[i]) {
            elements_to_display[elements_to_display.length] = list[i];
        }
    }

    for (let i = elements_to_display.length - 1; i >= 0; i--) {
        elements_to_display[i].style.display = "none";
        if (i < (elements_to_display.length - ((page - 1) * max_item)) && i >= (elements_to_display.length - (page * max_item)))
            elements_to_display[i].style.display = "flex";
    }
}

if (document.getElementById("pagination_suivant"))
    document.getElementById("pagination_suivant").addEventListener("click", suivant);

if (document.getElementById("pagination_precedent"))
    document.getElementById("pagination_precedent").addEventListener("click", precedent);

/* Filters */
function displayElements(countryname, area, duration, job, company, classname) {
    if (document.querySelector("." + classname)) {
        let elements_country_field = document.querySelectorAll("." + classname + " > * > * > * > .country-field");
        let elements_area_field = document.querySelectorAll("." + classname + " > * > * > * > * > .area-field");
        let elements_job_field = document.querySelectorAll("." + classname + " > * > * > .job-field");
        let elements_company_field = document.querySelectorAll("." + classname + " > * > * > * > .company-field");
        let elements_duration_field = document.querySelectorAll("." + classname + " > * > * > * > .duration-field");
        let elements = document.querySelectorAll("." + classname);

        /* FOR KEY WORD */
        if (document.getElementById("key_words")) {
            let key_word = document.getElementById("key_words").value;
            let key_word_nospace = key_word.split(" ").join("");
            if (key_word_nospace === "" && job === "Métier" && duration === "Durée" && company === "Entreprise" && area === "Secteur" && countryname === "Pays") {
                /* EMPTY INPUTS */
                for (let i = 0; i < elements.length; i++)
                    selected_element[i] = true
            }
            else if (key_word_nospace !== "" && job === "Métier" && duration === "Durée" && company === "Entreprise" && area === "Secteur" && countryname === "Pays") {
                /* KEYWORD AND EMPTY SELECT */
                for (let l = 0; l < elements.length; l++) {
                    if (elements_job_field[l]) {
                        if (elements_job_field[l].textContent.toLowerCase().search(key_word.toLowerCase()) !== -1) {
                            selected_element[l] = true;
                        }
                        else {
                            selected_element[l] = false;
                        }
                    }
                }
            }
            else if (key_word_nospace !== "") {
                /* KEYWORD AND NOT EMPTY SELECT */
                let associative_element = [];
                for (let i = 0; i < elements.length; i++)
                    associative_element[i] = true;

                /* FOR COUNTRIES */
                for (let i = 0; i < elements.length; i++) {
                    if (elements_country_field[i] !== undefined && countryname !== "Pays") {
                        if (elements_country_field[i].textContent.search(countryname) === -1) {
                            associative_element[i] = false;
                        }
                    }
                }
                /* FOR AREA */
                for (let i = 0; i < elements.length; i++) {
                    if (elements_area_field[i] !== undefined && area !== "Secteur") {
                        if (elements_area_field[i].textContent.search(area) === -1) {
                            associative_element[i] = false
                        }
                    }
                }

                /* FOR COMPANY */
                for (let i = 0; i < elements.length; i++) {
                    if (elements_company_field[i] !== undefined && company !== "Entreprise") {
                        if (elements_company_field[i].textContent.search(company) === -1) {
                            associative_element[i] = false;
                        }
                    }
                }
                /* FOR DURATION */

                for (let i = 0; i < elements.length; i++) {
                    if (elements_duration_field[i] && duration !== "Durée") {
                        if (elements_duration_field[i].textContent.search(duration) === -1) {
                            associative_element[i] = false
                        }
                    }
                }

                /* FOR JOB NAME */

                for (let i = 0; i < elements.length; i++) {
                    if (elements_job_field[i] !== undefined && job !== "Métier") {
                        if (elements_job_field[i].textContent.search(job) === -1) {
                            associative_element[i] = false;
                        }
                    }
                }

                for (let i = 0; i < elements.length; i++)
                    associative_element[i] ? selected_element[i] = true : selected_element[i] = false;

                for (let l = 0; l < elements.length; l++) {
                    if (elements_job_field[l]) {
                        if (elements_job_field[l].textContent.toLowerCase().search(key_word.toLowerCase()) === -1) {
                            selected_element[l] = false;
                        }
                    }
                }
            }
            else if (key_word_nospace === "") {
                /* EMPTY KEYWORD AND NOT EMPTY SELECT */
                let associative_element = [];
                for (let i = 0; i < elements.length; i++)
                    associative_element[i] = true;

                /* FOR COUNTRIES */
                for (let i = 0; i < elements.length; i++) {
                    if (elements_country_field[i] !== undefined && countryname !== "Pays") {
                        if (elements_country_field[i].textContent.search(countryname) === -1) {
                            associative_element[i] = false;
                        }
                    }
                }
                /* FOR AREA */
                for (let i = 0; i < elements.length; i++) {
                    if (elements_area_field[i] !== undefined && area !== "Secteur") {
                        if (elements_area_field[i].textContent.search(area) === -1) {
                            associative_element[i] = false
                        }
                    }
                }

                /* FOR COMPANY */
                for (let i = 0; i < elements.length; i++) {
                    if (elements_company_field[i] !== undefined && company !== "Entreprise") {
                        if (elements_company_field[i].textContent.search(company) === -1) {
                            associative_element[i] = false;
                        }
                    }
                }
                /* FOR DURATION */

                for (let i = 0; i < elements.length; i++) {
                    if (elements_duration_field[i] && duration !== "Durée") {
                        if (elements_duration_field[i].textContent.search(duration) === -1) {
                            associative_element[i] = false
                        }
                    }
                }

                /* FOR JOB NAME */

                for (let i = 0; i < elements.length; i++) {
                    if (elements_job_field[i] !== undefined && job !== "Métier") {
                        if (elements_job_field[i].textContent.search(job) === -1) {
                            associative_element[i] = false;
                        }
                    }
                }

                for (let i = 0; i < elements.length; i++)
                    associative_element[i] ? selected_element[i] = true : selected_element[i] = false;

            }
        }
    }
};

function hideElements(classname) {
    if (document.querySelector("." + classname)) {
        let elements = document.querySelectorAll("." + classname);
        for (let i = 0; i < elements.length; i++)
            elements[i].style.display = "none";
    }
};

if (document.querySelectorAll("#country-list") && document.querySelectorAll("#area-list") && document.querySelectorAll("#duration-list") && document.querySelectorAll("#job-list") && document.querySelectorAll("#company-list")) {
    if (document.querySelector("#search-validation-country")) {
        document.querySelector("#search-validation-country").addEventListener('click', (e) => {
            e.preventDefault();
            let country = document.getElementById("country-list").value;
            let area = document.getElementById("area-list").value;
            let duration = document.getElementById("duration-list").value;
            let job = document.getElementById("job-list").value;
            let company = document.getElementById("company-list").value;
            hideElements("filtered_element");
            displayElements(country, area, duration, job, company, "filtered_element");
            page = 1;
            update_element_length();
            display_pagination_link();
            display_pagination();
            display_element();
        })
    }
}

display_element();
update_element_length();
display_pagination_link();
display_pagination();