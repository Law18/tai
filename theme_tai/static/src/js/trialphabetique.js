function trialphabetique() {
    /* 
    *** TRI PAR ORDRE ALPHABÉTIQUE DE CHAMPS SELECT
    */
    // Récupération des select
    let all_select_alphabetical = document.querySelectorAll(".alphabetical-sort");
    /* 
    *** Récupérer les valeurs des options d'un select ***
    *** Stockage dans l'array options_alphabetical ***
    *** Tri automatique de l'array options_alphabetical ***
    */
    let options_alphabetical = [];
    for (let i = 0; i < all_select_alphabetical.length; i++) {
        options_alphabetical[i] = [];
        for (let j = 1; j < all_select_alphabetical[i].length; j++)
            options_alphabetical[i][j] = all_select_alphabetical[i][j].textContent;
        options_alphabetical[i].sort();
    }
    console.log();
    // Greffer les valeurs dans le html
    for (let i = 0; i < all_select_alphabetical.length; i++) {
        for (let j = 1; j < all_select_alphabetical[i].length; j++)
            all_select_alphabetical[i][j].innerHTML = options_alphabetical[i][j - 1];
    }
}
trialphabetique();