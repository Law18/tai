function updateTitleWidth() {
    if (document.querySelector(".actualite-card>main") && document.querySelectorAll(".actu-article-title>span") && document.querySelectorAll(".actu-article-subtitle>span")) {
        const titles = document.querySelectorAll(".actu-article-title>span");
        const subTitles = document.querySelectorAll(".actu-article-subtitle>span");
        for (let i = 0; i < titles.length; i++) {
            titles[i].style.width = document.querySelector(".actualite-card>main").clientWidth + "px";
            subTitles[i].style.width = document.querySelector(".actualite-card>main").clientWidth + "px";
        }
    }
}

if (document.querySelector("#tai_actualite_carousel>.carousel-inner>.carousel-item:first-child")) {
    document.querySelector("#tai_actualite_carousel>.carousel-inner>.carousel-item:first-child").classList.add("active");
}

updateTitleWidth();

window.addEventListener('resize', () => {
    updateTitleWidth();
});