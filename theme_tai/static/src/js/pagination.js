let currentPage = 1;
let allElements = [];
let elementPerPage = 4;

function taiRecoverElements(containerId, elementClass, elementPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && elementPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === elementPerPage || i === elementNb - 1) {
                allElements[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function taiDisplayElements() {
    for (let i = 0; i < allElements.length; i++) {
        for (let j = 0; j < allElements[i].length; j++) {
            if (i === currentPage - 1) {
                allElements[i][j].classList.add("d-flex");
                allElements[i][j].classList.remove("d-none");
            }
            else {
                allElements[i][j].classList.remove("d-flex");
                allElements[i][j].classList.add("d-none");
            }
        }
    }
}

function taiNext(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (currentPage < allElements.length) {
            currentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (currentPage === allElements.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        taiDisplayNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    taiDisplayElements();
}

function taiPrevious(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (currentPage > 1) {
            currentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (currentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        taiDisplayNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    taiDisplayElements();
}

function taiDisplayNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = currentPage + " / " + allElements.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && currentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && currentPage === allElements.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

taiRecoverElements("#rubric-container", ".rubric-element", elementPerPage);
taiDisplayElements();
taiDisplayNavigation("#rubric-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#rubric-pagination>.precedent") && document.querySelector("#rubric-pagination>.suivant")) {
    document.querySelector("#rubric-pagination>.precedent").addEventListener("click", () => {
        taiPrevious("#rubric-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#rubric-pagination>.suivant").addEventListener("click", () => {
        taiNext("#rubric-pagination", ".page-number", ".suivant", ".precedent")
    })
}