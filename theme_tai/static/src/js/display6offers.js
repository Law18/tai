const max_item_general = 6;
if (document.querySelector(".mission_general_by6_only")) {
    let list_general_offers = document.querySelectorAll(".mission_general_by6_only");

    for (let i = list_general_offers.length - 1; i >= 0; i--) {
        list_general_offers[i].style.display = "none";
        if (i < (list_general_offers.length - ((page - 1) * max_item_general)) && i >= (list_general_offers.length - (page * max_item_general)))
            list_general_offers[i].style.display = "flex";
    }
}
if (document.querySelector(".mission_country_by6_only")) {
    let list_general_offers = document.querySelectorAll(".mission_country_by6_only");

    for (let i = list_general_offers.length - 1; i >= 0; i--) {
        list_general_offers[i].style.display = "none";
        if (i < (list_general_offers.length - ((page - 1) * max_item_general)) && i >= (list_general_offers.length - (page * max_item_general)))
            list_general_offers[i].style.display = "flex";
    }
}

/* *************************************** */

/* Get country list */

function offerDisplayElements(countryname, classname) {
    if (document.querySelector("." + classname)) {
        let elements_country_field = document.querySelectorAll("." + classname + " > * > * > * > .country-field");
        let elements = document.querySelectorAll("." + classname);

        /* DEFAULT */
        if (countryname === "Pays") {
            /* EMPTY INPUTS */
            for (let i = 0; i < 6; i++) {
                if (elements_country_field[i] !== undefined) {
                    elements[i].style.display = "flex";
                }
            }
        }
        else {
            /* EMPTY KEYWORD AND NOT EMPTY SELECT */
            let associative_element = [];
            let limite = 0;
            for (let i = 0; i < elements.length; i++)
                associative_element[i] = true;

            /* FOR COUNTRIES */
            for (let i = 0; i < elements.length; i++) {
                if (elements_country_field[i] !== undefined && countryname !== "Pays") {
                    if (elements_country_field[i].textContent.search(countryname) === -1) {
                        associative_element[i] = false;
                    }
                }
            }
            /* DISPLAY */
            for (let i = 0; i < elements.length; i++) {
                if (limite < 6) {
                    associative_element[i] ? elements[i].style.display = "flex" : elements[i].style.display = "none";
                    limite++;
                }
            }
        }
    }
};

function offerHideElements(classname) {
    if (document.querySelector("." + classname)) {
        let elements = document.querySelectorAll("." + classname);
        for (let i = 0; i < elements.length; i++)
            elements[i].style.display = "none";
    }
};

if (document.querySelector("#mission-offer-country-list")) {
    document.querySelector("#mission-offer-country-list").addEventListener('click', (e) => {
        e.preventDefault();
        let country = document.getElementById("mission-offer-country-list").value;
        console.log(country);
        offerHideElements("filtered_by_country");
        offerDisplayElements(country, "filtered_by_country");
    })
}