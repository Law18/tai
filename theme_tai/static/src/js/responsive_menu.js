let open = false;
if (document.getElementById("mobile_menu")) {
    document.getElementById("mobile_menu").addEventListener("click", () => {
        if (document.getElementById("mobile_nav")) {
            open ? document.getElementById("mobile_nav").style.left = "-100%" : document.getElementById("mobile_nav").style.left = "0%";
            open = !open;
        }
    })
}