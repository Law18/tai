/* const accordionQuestions = document.querySelectorAll(".area>.question");
var collection;
accordionQuestions.forEach(accordionQuestion => {
    accordionQuestion.addEventListener("click", event => {
    accordionQuestion.classList.toggle("active");
    const accordionItemBody = accordionQuestion.nextElementSibling;
    collection = accordionQuestion.children;
    if(accordionQuestion.classList.contains("active")) {
      accordionItemBody.style.display = "block";
      accordionItemBody.style.backgroundColor = "#1a3f92";
      accordionItemBody.style.color = "#fff";
      accordionItemBody.parentElement.style.backgroundColor = "#1a3f92";
      for (let i = 0; i < collection.length; i++) {
        collection[i].style.color = "#fff";
      }
      collection[2].textContent = "x";
      collection[2].style.color = "#1a3f92";
      collection[2].style.fontSize = "26px" ;
    }
    else {
      accordionItemBody.style.display = "none";
      accordionItemBody.parentElement.style.backgroundColor = "#e5efff";
      collection[0].style.color = "#1a3f92";
      collection[1].style.color = "rgba(25, 47, 98, 0.63)";
      collection[2].textContent = "+";
      collection[2].style.color = "#1a3f92";
      collection[2].style.fontSize = "28px" ;
    }
  });
});

let list_faq = document.querySelectorAll("." + html_class);

if (document.querySelector("#search-validation-keyword_faq")) {
    document.querySelector("#search-validation-keyword_faq").addEventListener('click', (e) => {
        e.preventDefault();
        let key_word = document.getElementById("faq_key_words").value;
        displayQuestions();
    })
}

function hideQuestions(questions_selected) {
    if (document.querySelector(".question")) {
        let ok;
        let questions = document.querySelectorAll(".question");
        let contenu_questions = document.querySelectorAll(".question>.BlockTitle");
    
        for (let i = 0; i < contenu_questions.length; i++){
        ok = 0;
            for(let j = 0; j < questions_selected.length; j++){
                if(contenu_questions[i] === questions_selected[j]) {
                    ok = 1;
                }
            }
            if(ok === 0) {
                questions[i].parentElement.outerHTML="";
            }
              
        }
    }   
}


function displayQuestions() {
        let questions_selected = [];
        let elements_question_field = document.querySelectorAll(".question>.BlockTitle");
        let key_word_faq = document.getElementById("faq_key_words").value;
        let key_word_nospace_faq = key_word_faq.split(" ").join("");

       if (key_word_nospace_faq !== ""){
        for (let l = 0; l < elements_question_field.length; l++) {
            if (elements_question_field[l]) {
                if (elements_question_field[l].textContent.toLowerCase().search(key_word_faq.toLowerCase()) !== -1) {
                    questions_selected[l] = elements_question_field[l];
                } 
            }
        }
        hideQuestions(questions_selected);
     }
}
 */

if (document.querySelector(".btn-faq-rubrique") && document.querySelector(".collapse-faq-rubrique") && document.querySelector(".btn-faq-rubrique-question") && document.querySelector(".collapse-faq-rubrique-reponse") && document.querySelector(".collapse-faq-rubrique-question") && document.querySelector(".faq-questions")) {
    let btnCollapsesRubrique = document.querySelectorAll(".btn-faq-rubrique");
    let rubriqueCollapses = document.querySelectorAll(".collapse-faq-rubrique");
    //let limitQuestion = document.querySelectorAll(".collapse-faq-rubrique>.limit-questions-5");
    for (let i = 0; i < rubriqueCollapses.length; i++) {
        rubriqueCollapses[i].id = `rubriqueCollapses${i}`;
        btnCollapsesRubrique[i].setAttribute(`data-target`, `#rubriqueCollapses${i}`);
        btnCollapsesRubrique[i].addEventListener("click", () => {
            rubriqueCollapses[i].style.backgroundColor === "#e5efff" ?
                rubriqueCollapses[i].style.backgroundColor = "#1a3f92" : rubriqueCollapses[i].style.backgroundColor = "#e5efff"
        })
    }

    let btnCollapsesQuestion = document.querySelectorAll(".btn-faq-rubrique-question");
    let reponsesCollapses = document.querySelectorAll(".collapse-faq-rubrique-reponse");
    let questionCollapses = document.querySelectorAll(".collapse-faq-rubrique-question");
    let question = document.querySelectorAll(".faq-questions");

    for (let i = 0; i < reponsesCollapses.length; i++) {
        reponsesCollapses[i].id = `reponseCollapses${i}`;
        btnCollapsesQuestion[i].setAttribute(`data-target`, `#reponseCollapses${i}`);
        btnCollapsesQuestion[i].addEventListener("click", () => {
            if (btnCollapsesQuestion[i].textContent === "+") {
                btnCollapsesQuestion[i].innerHTML = "-";
                questionCollapses[i].style.backgroundColor = "#1a3f92";
                question[i].style.color = "white";
            }
            else {
                btnCollapsesQuestion[i].innerHTML = "+";
                questionCollapses[i].style.backgroundColor = "#e5efff";
                question[i].style.color = "#64769c";
            }
        })
        /* console.log(questionCollapses[i].classList.contains("limit-questions-5"));
        if (i >= 5 && questionCollapses[i].classList.contains("limit-questions-5")) {
            questionCollapses[i].style.display = "none";

        } */
    }
}