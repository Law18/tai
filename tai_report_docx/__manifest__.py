# -*- coding: utf-8 -*-
{
    "name" : "Tai docx report",
    "version" : "1.0",
    "author" : "Tugende",
    "category": "Hidden",
    "description": """Create report from docx templates""",
    'website': 'https://gotugende.com/',
    "category": "Reporting",
    "development_status": "Production/Stable",
    "external_dependencies": {"python": ['docxtpl']},
    "depends": ["base", "web"],
    "demo": [],
    "installable": True,
    "assets": {
        "web.assets_backend": [
            "tai_report_docx/static/src/js/report/action_manager_report.esm.js",
        ],
    }
}