#-*- coding:utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError
import os
try:
    from docxtpl import DocxTemplate
    from docx.opc.exceptions import PackageNotFoundError
except ImportError:
    pass

import uuid
import logging
log = logging.getLogger('mes logs')

class ReportAction(models.Model):
    _inherit = "ir.actions.report"

    report_type = fields.Selection(
        selection_add=[("docx", "DOCX")], ondelete={"docx": "set default"}
    )


    @api.model
    def _render_docx(self, docids, data):
        log.error('je rentre dans R docx')
        report_model_name = "report.%s" % self.report_name
        report_model = self.env.get(report_model_name)
        if report_model is None:
            raise UserError(_("%s model was not found") % report_model_name)
        tmp_folder = "/tmp"
        file_uid = str(uuid.uuid4())
        full_docx_path = tmp_folder + '/' + file_uid + ".docx"
        converted_docx_path = tmp_folder + '/' + file_uid + "_converted.docx"
        pdf_path = tmp_folder + '/' + file_uid + "_converted.pdf"
        # FIXME: Handle printing multiple documents
        doc_id = docids[0]
        self._save_file(full_docx_path, report_model.get_template_file(doc_id, data).raw)
        doc = DocxTemplate(full_docx_path)
        try:
            doc.render(report_model.get_template_data(doc_id, data))
        except PackageNotFoundError:
            raise UserError("Cannot open the template file. Please check that it is a Word document.")
        doc.save(converted_docx_path)
        self._convert_docx_to_pdf(tmp_folder, converted_docx_path)
        f = open(pdf_path, "rb")
        pdf = f.read()
        f.close()
        os.remove(full_docx_path)
        os.remove(converted_docx_path)
        os.remove(pdf_path)
        return (
            pdf, "docx"
        )

    @api.model
    def _get_report_from_name(self, report_name):
        res = super(ReportAction, self)._get_report_from_name(report_name)
        if res:
            return res
        report_obj = self.env["ir.actions.report"]
        qwebtypes = ["docx"]
        conditions = [
            ("report_type", "in", qwebtypes),
            ("report_name", "=", report_name),
        ]
        context = self.env["res.users"].context_get()
        return report_obj.with_context(**context).search(conditions, limit=1)

    def _convert_docx_to_pdf(self, tmp_folder_name, docx_path):
        output_path = tmp_folder_name

        cmd = "soffice --headless --convert-to pdf --outdir " + output_path \
            + " " + docx_path
        os.system(cmd)
    
    def _save_file(self, folder_name, file):
        out_stream = open(folder_name, 'wb')
        try:
            out_stream.write(file)
        finally:
            out_stream.close()