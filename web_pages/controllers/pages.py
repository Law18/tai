from odoo import http
from odoo.http import request

class Pages(http.Controller):
    @http.route('/web/home', auth='public',type='http',website=True)
    def web_home(self, **kw):
        return request.render('web_pages.tai_home_page', {})

    @http.route('/web/mission', auth='public',type='http',website=True)
    def web_misson(self, **kw):
        return request.render('web_pages.tai_mission_page', {})
