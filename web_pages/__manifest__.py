{
    'name': 'tai web page',
    'description': 'Page web TAI',
    'category': 'Website/Website',
    'summary': 'Pages web TAI',
    'version': '1.0',
    'author': 'Baamtu',
    'depends': ['base', 'web', 'website'],
    'data': [
        'views/tai_home_template.xml',
        'views/tai_mission_template.xml',
    ],
    'license': 'LGPL-3',
    'assets': {
        'web.assets_frontend': [
            'web_pages/static/src/css/tai.css',
            'web_pages/static/src/css/tai_header.css',
            'web_pages/static/src/css/tai_sm_links.css',
            'web_pages/static/src/css/tai_button.css',
            'web_pages/static/src/css/tai_carousel.css',
            'web_pages/static/src/css/tai_footer.css',
            'web_pages/static/src/css/tai_general.css',
            'web_pages/static/src/css/tai_goals.css',
            'web_pages/static/src/css/tai_main.css',
            'web_pages/static/src/css/tai_mission.css',
            'web_pages/static/src/css/tai_search_missions.css',
            'web_pages/static/src/css/tai_section.css',
            'web_pages/static/src/css/tai_title.css',
        ],
    }
}
