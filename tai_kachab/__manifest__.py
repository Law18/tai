# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'TAI',
    'version': '1.0',
    'category': 'Uncategorized',
    'description': """
Projet TAI
""",
    'website': "http://www.baamtu.com",
    'depends': ['base', 'contacts'],
    'author': "Baamtu",
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/partner_views.xml'
    ],
    'application': False,
    'installable': True,
    'auto_install': False,
}
