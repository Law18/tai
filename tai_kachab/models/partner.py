# -*- coding: utf-8 -*-f
from odoo import models, fields, api

class Partner(models.Model):

    _inherit = 'res.partner'
    
    _AGREMENTS_STATES = [
        ('draft', 'Nouveau'),
        ('accepted', 'Acceptée'),
        ('rejected', 'Rejetée'),  
    ]
    
    # Caractéristiques
    is_manager = fields.Boolean(string='Gestionnaire', default=False, compute="_compute_manager")
    state = fields.Selection(selection=_AGREMENTS_STATES, string=u'status', default="draft", required=True)
    
    def _compute_manager(self):
        print('FONCTION MANAGER')
        if self.env.user.has_group('tai.group_res_partner_manager'):
            self.is_manager = True
        else:
            self.is_manager = False
        print(self.is_manager)
        
    def button_accepted(self):
        
        return self.write({"state": "accepted"})
    
    def button_rejected(self):
        return self.write({"state": "rejected"})

"""     @api.model
    def create(self, vals):
        print("*******************************************************************************")
        res= super(Partner, self).create(vals)
        print(self.email)
        self.env['res.users'].sudo().create({
            'partner_id': res.id,
            'name': self.name,
            'login': self.email,
            'company_id': self.company_id.id
        })
        return res """
    