# -*- coding: utf-8 -*-
{
    'name' : 'Baamtu pièces jointes',
    'version': '1.0',
    'author' : 'Baamtu',
    'website' : 'http://www.baamtu.com',
    'category': 'attachment',
    'depends' : ['base'],
    'description': """
Module permettant de gérer les pièces jointes
=========================================================
Il permet notamment de forcer le stockage des fichiers sur le filesystem.

    """,
    'data': [
        'data/battachment_data.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
