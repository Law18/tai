# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo import tools
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools.translate import _
import os

class battachment(models.Model):
    _inherit = "ir.attachment"

    @api.model
    def _get_dir(self):
        battachment_data = self.env['ir.config_parameter'].get_param('b_filestore')
        return battachment_data

    # We want to force the storage of attachments in the filesytem
    @api.model
    def _full_path(self, path):
        return os.path.join(self._get_dir(), self._cr.dbname, path)

    # We overide the _data_set method in order to force the config var "ir_attachment.location" to filestore.
    # By default, openerp stores attachments in database. This behaviour can be changed in the admin settings (Settings->Technical->Parameters-System parameters)
    # However, a human action is needed. In our case we know for sure that we're going to store files in filesystem so we force this in the code to avoid human errors
    def _inverse_datas(self):

        if (os.path.exists(self._get_dir())):
            location = self.env['ir.config_parameter'].get_param('ir_attachment.location')
            if not location:
                self.env['ir.config_parameter'].set_param('ir_attachment.location', 'filestore')
        else :
            raise ValidationError("Le serveur ne dispose pas d'espace de stockage prévu à cet effet")

        return super(battachment, self)._inverse_datas()

    # override of _data_get in ir_attachment
    def _compute_datas(self):
        return super(battachment, self)._compute_datas()

    datas = fields.Binary(compute=_compute_datas, inverse=_inverse_datas, string='File Content')


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
