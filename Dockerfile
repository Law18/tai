FROM odoo:15.0

USER root

RUN apt update
RUN apt install -y unoconv vim git watchdog locales locales-all
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE fr_FR.UTF-8
RUN pip install cachetools==3.1.1
RUN git clone https://github.com/keshrath/pdfconv.git \
    && chown -R odoo:odoo /pdfconv \
    && chmod a+wr -R /tmp

WORKDIR /pdfconv/

WORKDIR /

COPY entrypoint.sh /
COPY odoo.conf /etc/odoo/
RUN chown odoo /etc/odoo/odoo.conf && chmod 777 /etc/odoo/odoo.conf
RUN chown odoo entrypoint.sh && chmod 777 entrypoint.sh

#Setup odoo log directory
RUN mkdir -p /var/log/odoo && chown -R odoo /var/log/odoo
RUN mkdir -p /mnt/tai && chown -R odoo /mnt/tai

#Setup directory for odoo attachements
RUN mkdir /home/odoo_filestore && chmod -R 777 /home/odoo_filestore

VOLUME ["/var/log/odoo", "/home/odoo_filestore"]

COPY web_auth_oauth /mnt/web_auth_oauth
COPY web_auth_signup /mnt/web_auth_signup
COPY web_portal /mnt/web_portal

USER odoo
