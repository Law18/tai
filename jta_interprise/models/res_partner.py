# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = ['res.partner']

    _AGREMENTS_STATES = [
        ('draft', 'Nouveau'),
        ('accepted', 'Acceptée'),
        ('rejected', 'Rejetée'),
    ]

    birth_city = fields.Char('City of birth')
    birth_country = fields.Many2one('res.country', 'Country of birth')
    nationality = fields.Many2one('res.country', 'Nationality')
    birthdate = fields.Date('Date of birth')
    civility_id = fields.Many2one('res.partner.title', 'Civility')
    gender_id = fields.Many2one('res.partner.sex', 'Genre')
    residence_country = fields.Many2one(
        comodel_name='res.country',
        string='Country of residence',
        domain=[('is_umoa', '=', True)])

    lang_ids = fields.One2many('res.partner.lang.level', 'partner_id', string='Spoken languages')
    xperience_ids = fields.One2many('res.partner.xperience', 'partner_id')
    school_curriculum_ids = fields.One2many('res.partner.school_curriculum', 'partner_id')

    activity_area_id = fields.Many2one('activity.area', string="Secteur d'activité")
    enterprise_type_id = fields.Many2one('enterprise.type', string="Type d'enterprise")
    creation_date = fields.Date(u'Date de création')
    staff = fields.Integer(u'Effectif salarié')
    vat = fields.Char(string=u"Numéro d'identification")
    trade_register= fields.Char(string="N° RC")
    has_experiences = fields.Boolean('Has experiences')
    description_entreprise = fields.Html(string='Description')
    professional_activity = fields.Char(string=u"Activité professionelle")
    unit_manager = fields.Boolean(string =u"Chef d'unité")
    # Caractéristiques
    is_manager = fields.Boolean(string='Gestionnaire', default=False, compute="_compute_manager")
    state = fields.Selection(selection=_AGREMENTS_STATES, string=u'status', default="draft",track_visibility='onchange', required=True)    
    firstname = fields.Char(string=u"Prénom")
    lastname = fields.Char(string=u"Nom")
    default_company_officer = fields.Boolean(string=u"Représentant par défaut", default=True)
    display_testimonial = fields.Boolean(u'Afficher témoignage', default=False, domain=[('user_id.user_type', '=', 'jta')])
    testimonial = fields.Html(u'Témoignage')
    user_type = fields.Selection(related='user_ids.user_type')
            
    def _compute_manager(self):
        if self.env.user.has_group('jta_interprise.group_res_partner_manager'):
            self.is_manager = True
        else:
            self.is_manager = False

    def button_accepted(self):
      
        # user_ids = self.child_ids.filtered(lambda t: t.default_company_officer == True).user_ids
        user_ids = self.child_ids
        user = self.env['res.users'].search([('active', '=', False), ('partner_id', 'in', user_ids.mapped('id'))])      
        user.write({'active': True, 'registration_state': "accepted"})
        self.write({"state": "accepted"})
        template = self.env.ref('jta_interprise.mail_template_enterprise_signup_request_accepted',raise_if_not_found=False)              
        template.sudo().send_mail(self.id, force_send=True)
      
        
    def button_rejected(self):
        user_ids = self.child_ids
        user = self.env['res.users'].search([('active', '=', False), ('partner_id', 'in', user_ids.mapped('id'))])
        user.write({'active': False, 'registration_state': "rejected"})
        self.write({"state": "rejected"})

    def write(self, values):
        res = super(ResPartner, self).write(values)

        """ Permettre au representant par défaut de créer ou editer des enregistrement de type utilisateur (res.users)"""
        if not self.is_company:
            group_e = self.env.ref('jta_interprise.group_res_partner_agent', False)
            if 'default_company_officer' in values:
                if not values.get('default_company_officer'):
                    group_e.sudo().write({'users': [(3, self.id)]})
                else:
                    group_e.sudo().write({'users': [(4, self.user_ids.id)]})
        return res
    
    def filter_enterprise(self):
        domain = [('is_company','=',True),('state','=','accepted'),('country_id.id','=',self.env.user.partner_id.country_id.id)]
        if self.env.user.has_group('base.group_system'):
            domain = [('is_company','=',True),('state','=','accepted')]
        view_id = self.env.ref('base.res_partner_kanban_view').id
        form_id = self.env.ref('base.view_partner_form').id
        list_id = self.env.ref('base.view_partner_tree').id
        return {'type': 'ir.actions.act_window',
                'name': 'Entreprises agréées',
                'res_model': 'res.partner',
                'view_mode': 'kanban',
                'views': [[view_id, 'kanban'],[form_id,'form'],[list_id,'tree']],
                'domain': domain   
            }
        
    def filter_representative(self):
        domain = [('is_company','=',False),('parent_id.id', '=', self.env.user.partner_id.parent_id.id)]    
        if self.env.user.has_group('base.group_system'):
            domain = [('is_company','=',False),('user_ids.user_type','=','representative'),('unit_manager','=',False)]  
        view_id = self.env.ref('base.res_partner_kanban_view').id
        form_id = self.env.ref('base.view_partner_form').id
        list_id = self.env.ref('base.view_partner_tree').id
        return {'type': 'ir.actions.act_window',
                'name': 'Représentants',
                'res_model': 'res.partner',
                'view_mode': 'kanban',
                'views': [[view_id, 'kanban'],[form_id,'form'],[list_id,'tree']],
                'domain': domain   
            }
        
    def filter_approval(self):
        domain = [('is_company','=',True),('state','=','draft'),('country_id.id','=',self.env.user.partner_id.country_id.id)]
        if self.env.user.has_group('base.group_system'):
            domain = [('is_company','=',True),('state','=','draft')]
        view_id = self.env.ref('base.res_partner_kanban_view').id
        form_id = self.env.ref('base.view_partner_form').id
        list_id = self.env.ref('base.view_partner_tree').id
        return {'type': 'ir.actions.act_window',
                'name': "Les nouvelles demandes d'agrément",
                'res_model': 'res.partner',
                'view_mode': 'kanban',
                'views': [[view_id, 'kanban'],[form_id,'form'],[list_id,'tree']],
                'domain': domain   
            }
        
class PartnerSex(models.Model):
    _name = 'res.partner.sex'
    _description = 'Partner Sex'

    name = fields.Char('Sex')


class ActivityArea(models.Model):
    _name = 'activity.area'
    _description = 'Activity Area'

    name = fields.Char('Name')


class StudyLevel(models.Model):
    _name = 'study.level'
    _description = ''

    name = fields.Char('Name')


class PartnerCurriculum(models.Model):
    _name = 'res.partner.school_curriculum'
    _description = ''

    partner_id = fields.Many2one('res.partner', 'Partner')
    study_level_id = fields.Many2one('study.level', 'Level of study')
    establishment = fields.Char('Establisshment')
    mention = fields.Char('Mention')
    diploma = fields.Char('Diploma')
    start_date = fields.Date('Année de début')
    obtained_date = fields.Char('Date d\'obtention')
    end_date = fields.Date('Année de fin')
    actual = fields.Boolean('A pésent')
    country_id = fields.Many2one('res.country', 'Country')
    city = fields.Char('City')


class PartnerXperience(models.Model):
    _name = 'res.partner.xperience'
    _description = 'Partner Expérience'
    _rec_name = 'company_name'

    partner_id = fields.Many2one('res.partner', 'Partner')
    country_id = fields.Many2one('res.country', 'Country')
    city = fields.Char('City')
    company_name = fields.Char('Société')
    title = fields.Char('Fonction')
    start_date = fields.Date('Année de début')
    end_date = fields.Date('Année de fin')
    actual = fields.Boolean('A pésent')
    contract_type_id = fields.Many2one('contract.type', string='Contract Type')


class PartnerLangLevel(models.Model):
    _name = 'res.partner.lang.level'
    _description = 'Language level'

    partner_id = fields.Many2one('res.partner', 'Partner')
    level_id = fields.Many2one('res.lang.level', 'Name')
    lang_id = fields.Many2one('res.lang', 'Name')


class ContractType(models.Model):
    _name = 'contract.type'
    _description = 'Contract Type'

    name = fields.Char(required=True)