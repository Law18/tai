from odoo import models, fields

class EnterpriseType(models.Model):
    _name = 'enterprise.type'
    _description = 'Enterprise type'

    name = fields.Char(string = "Type d'entreprise")