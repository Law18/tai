# -*- coding: utf-8 -*-

from odoo import models, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    user_type = fields.Selection([
        ('jta', "JTA"),
        ('representative', 'Company representative')], string="Type d'utilisateur TAI", default='representative')