# -*- coding: utf-8 -*-

from odoo import models, fields


class ResCountry(models.Model):
    _inherit = 'res.country'

    is_umoa = fields.Boolean('Pays de l\'UEMOA')
    indemnity = fields.Integer(u'Indemnité')