#-*- coding:utf-8 -*-
from odoo import models, fields, api

class ResCompany(models.Model):
    _inherit = 'res.company'

    agreement_template_id = fields.Many2one('ir.attachment', string='Template')