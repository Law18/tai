# -*- coding: utf-8 -*-

from odoo import models, fields


class PartnerLangLevel(models.Model):
    _name = 'res.partner.lang.level'
    _description = 'Language level'

    partner_id = fields.Many2one('res.partner', 'Partner')
    level_id = fields.Many2one('res.lang.level', 'Name')
    lang_id = fields.Many2one('res.lang', 'Name')


class LangLevel(models.Model):
    _name = 'res.lang.level'
    _description = 'Language level'

    name = fields.Char('Name')