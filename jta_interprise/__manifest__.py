# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'JTA/Enterprise Contacts',
    'category': 'Sales/CRM',
    'summary': '',
    'description': """

""",
    'data': [
        'security/security.xml', 
        'security/ir.model.access.csv', 
        'data/data.xml',
        'views/res_partner.xml',
        'views/res_country.xml',
        'views/res_enterprise_type.xml',
        'views/res_partner_views.xml',
        'views/res_company.xml',
        'report/report_menu.xml',
        'data/email_template.xml',
        
       
    ],
    'depends': ['contacts', 'auth_signup'],
    'license': 'LGPL-3',
}