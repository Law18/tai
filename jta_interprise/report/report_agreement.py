#-*- coding:utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class ReportAgreement(models.AbstractModel):
    _name = 'report.agreement.docx'
    
    def get_template_file(self, docid, data):
        company_id = self.env['res.company'].search([])
        template_id = company_id.agreement_template_id
        if template_id:
            return template_id
        else:
            raise ValidationError(_("Il n'y a pas de template.Veuillez en ajouter un!"))
    
    
    def get_template_data(self, docid, data):
        # Do data formatting needed here
        agreement = self.env['res.partner'].browse(docid)
        representative = self.env['res.partner'].search([('default_company_officer','=',True),('id','in',agreement.mapped('child_ids.id'))],limit=1)

        return {'enterprise_type_id'     : agreement.enterprise_type_id.name,
                'name'     : agreement.name,
                'country_id'     : agreement.country_id.name,
                'city'     : agreement.city,
                'street'   : agreement.street,
                'creation_date'   : agreement.creation_date,
                'activity_area_id'   : agreement.activity_area_id.name,
                'staff'   : agreement.staff,
                'function'   :  representative.function,
                'representative_name'   : representative.name,
                'country_id_r'   : representative.country_id.name ,
                'city_r'   : representative.city,
                'phone'   : representative.phone ,
        }
    
    def _get_report_values(self, docids, data=None):
        return {
            'doc_ids' : docids,
            'doc_model' : self.env['res.partner'],
            'data' : data,
        }

