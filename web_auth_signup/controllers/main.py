# -*- coding: utf-8 -*-

import logging
import werkzeug
from odoo import http, _
from odoo.http import request
from odoo.exceptions import UserError
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.addons.web.controllers.main import SIGN_UP_REQUEST_PARAMS

_logger = logging.getLogger(__name__)


class WebAuthSignupHome(AuthSignupHome):

    @http.route('/web/type-partner-signup', type='http', auth='public', website=True, sitemap=False)
    def web_pre_auth_signup(self, **post):
        if post and request.httprequest.method == 'POST':
            return request.redirect('/web/signup')
        return request.render('web_auth_signup.pre_auth_signup', {})

    @http.route()
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                # Send an account creation confirmation email
                # if qcontext.get('token'):
                User = request.env['res.users']
                user_sudo = User.sudo().search(
                    User._get_login_domain(qcontext.get('login')), order=User._get_login_order(), limit=1
                )
                template = request.env.ref('auth_signup.mail_template_user_signup_account_created',
                                           raise_if_not_found=False)
                if user_sudo and template:
                    template.sudo().send_mail(user_sudo.id, force_send=True)

            except UserError as e:
                qcontext['error'] = e.args[0]
            except (SignupError, AssertionError) as e:
                if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                    qcontext["error"] = _("Another user is already registered using this email address.")
                else:
                    _logger.error("%s", e)
                    qcontext['error'] = _("Could not create a new account.")
            return request.redirect('/testimoney')

        response = request.render('auth_signup.signup', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    def get_auth_signup_qcontext(self):
        """ Add fields (phone, etc...) in Shared helper """

        SIGN_UP_REQUEST_PARAMS.update({'phone', 'firstname', 'lastname'})
        qcontext = super().get_auth_signup_qcontext()

        if request.params.get('firstname') and request.params.get('lastname'):
            qcontext.update({'name': request.params.get('firstname')+" "+request.params.get('lastname')})

        return qcontext

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = self._prepare_signup_values(qcontext)
        self._signup_with_values(qcontext.get('token'), values)
        # request.env.cr.commit()

    def _signup_with_values(self, token, values):
        db, login, password = request.env['res.users'].sudo().signup(values, token)
        # request.env.cr.commit()     # as authenticate will use its own cursor we need to commit the current transaction
        # uid = request.session.authenticate(db, login, password)
        # if not uid:
        #     raise SignupError(_('Authentication Failed.'))
