# -*- coding: utf-8 -*-
{
    'name': "web_auth_signup",
    'description': """
        Allow users to sign up and reset their  / With new fields
===============================================
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Hidden/Tools',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web', 'website', 'auth_oauth', 'auth_signup'],
    'license': 'LGPL-3',

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/auth_signup_login_template.xml',
    ],

    'assets': {
        'web.assets_frontend': [
            'web_auth_signup/static/src/js/signup.js'
        ],
    },
}
