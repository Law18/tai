# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError


class HrJob(models.Model):
    _inherit = 'hr.job'

    @api.model
    def default_get(self, fields):
        res = super(HrJob, self).default_get(fields)
        res['user_id'] = self._context.get('uid')
        return res

    active = fields.Boolean('Active', default=True)
    start_date_mission = fields.Date(string=u"Date de début")
    end_date_mission = fields.Date(string="Date de fin", compute="set_end_date_mission",store=True)
    start_date_offer = fields.Date(string="Date de publication de l'offre", compute="set_date_published",store=True)
    end_date_offer = fields.Date(string="Date de fin de l'offre")
    entreprise = fields.Many2one('res.partner', string='Entreprise', 
                                 domain=lambda self:[('is_company','=',True),('id','=',self.env.user.partner_id.parent_id.id)],  
                                default=lambda self:self.env.user.partner_id.parent_id)
    website_published = fields.Boolean(string='Est publié', readonly=True)
    website_id = fields.Many2one('website', string='Site web')
    duration_mission = fields.Selection(selection=[('12','12 mois'),('18','18 mois'),('24','24 mois')],string=u'Durée de la mission')
    address_id = fields.Char(string="Lieu de la mission" , default=lambda self:self.env.user.partner_id.street,
        help="Address where employees are working")
    presentation_mission = fields.Html(string=u'Présentation mission')
    level_of_study = fields.Selection(selection=[('1','Bac'),('2','Bac +2'),('3','Bac +3'),('4','Bac +4'),('5','Bac +5'),('6','Bac +5 et plus')],string=u'Niveau d’étude souhaité')
    ideal_profile = fields.Html(string=u'Profil idéal')
    applied = fields.Boolean(compute='_compute_applied',inverse='_inverse_is_applied')
    applied_user_ids = fields.Many2many('res.users', 'job_applied_user_rel', 'job_id', 'user_id')
    website_url = fields.Char('Website URL', compute='_compute_website_url')

    def _compute_website_url(self):
        super(HrJob, self)._compute_website_url()
        for job in self:
            job.website_url = "/web/mission/detail/%s" % job.id
   
    def _compute_applied(self):
        for job in self:
            job.applied = self.env.user in job.applied_user_ids
            
    def _inverse_is_applied(self):
        unapplied_jobs = applied_jobs = self.env['hr.job']
        for job in self:
            if self.env.user in job.applied_user_ids:
               unapplied_jobs |= job
            else:
                applied_jobs |= job
        applied_jobs.write({'applied_user_ids': [(4, self.env.uid)]})
        unapplied_jobs.write({'applied_user_ids': [(3, self.env.uid)]})

    state = fields.Selection([
        ('recruit', 'Recruitment in Progress'),
        ('open', 'Not Recruiting'),
        ('draft', 'Brouillon'),
        ('confirmed', u'Validé'),
        ('opened', u'Lancé'),
        ('finished', u'Terminé'),
        ('cancelled', u'Annulé')
    ], string='Status', readonly=True, required=True, tracking=True, copy=False, default='draft',
        help="Set whether the recruitment process is open or closed for this job position.")
    country_id = fields.Many2one('res.country', 'Pays',domain=[('is_umoa','=',True)] )
    remuneration = fields.Integer(string='Indemnité', related='country_id.indemnity' )

    def action_draft(self):
        self.state = 'draft'

    def action_confirm(self):
        self.state = 'confirmed'

    def action_cancel(self):
        self.state = 'cancelled'
    
    def action_finished(self):
        self.state = 'finished'
        self.write({'website_published': False})

    def action_finish(self):
        jobs = self.env['hr.job'].search([('end_date_offer','!=',False),('state','=','opened'),('end_date_offer','<=',date.today().strftime('%Y-%m-%d'))])
        for job in jobs:
            job.state = 'finished'
            job.write({'website_published': False})
            

    def action_open(self):
        self.state = 'opened'
        self.write({'website_published': True})
        self.start_date_offer = date.today().strftime('%Y-%m-%d')

    def set_open(self):
        self.write({'website_published': False})
        self.start_date_offer = fields.Date.today
        return super(HrJob, self).set_open()

    # update automatique du champ star_date_mission quand la mission est publiée
    @api.depends("website_published")
    def set_date_published(self):
        for record in self:
            if record.website_published:
                record.start_date_offer=date.today().strftime('%Y-%m-%d')
                record.state = 'opened'
                

    # calcul automatique de la fin de mission
    @api.depends("duration_mission")
    def set_end_date_mission(self):
        for record in self: 
            if record.start_date_mission:
                dt =record.start_date_mission + relativedelta(months=int(record.duration_mission))
                record.end_date_mission = fields.Datetime.to_string(dt)
                
    
    def filter_job_positions(self):
        domain = [('entreprise.country_id.id', '=', self.env.user.partner_id.country_id.id)]
        tree_id = self.env.ref('hr.view_hr_job_tree').id
        view_id = self.env.ref('hr_recruitment.view_hr_job_kanban').id
        form_id = self.env.ref('hr.view_hr_job_form').id
        return {'type': 'ir.actions.act_window',
                'name': 'Postes',
                'res_model': 'hr.job',
                'view_mode': 'kanban',
                'views': [[view_id, 'kanban'],[form_id,'form'],[tree_id,'tree']],
                'domain': domain   
            }  
         
    def unlink(self):
        for record in self:
            if record.state == 'opened':
                raise ValidationError(u"Vous ne pouvez pas supprimer une offre déjà lancé!")    
            return super(HrJob, self).unlink()    
           
   