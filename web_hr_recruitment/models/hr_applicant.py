import string
from odoo import models, fields, api
from odoo.http import request
import logging
Logger = logging.getLogger(__name__)

class HrApplicant(models.Model):
    _inherit = "hr.applicant" 
    
    civility = fields.Many2one("res.partner.title",string = "Civilité", compute='_compute_partner_adress')
    personal_address = fields.Char(string='Adresse personnelle', compute='_compute_partner_adress')
    country_birth = fields.Char(string="Pays de naissance", compute='_compute_partner_adress')
    country_residence = fields.Char(string="Pays de résidence", compute='_compute_partner_adress')
    type_id = fields.Char(string = "Niveau d'étude")
    xperience_ids = fields.One2many('res.partner.xperience', related="partner_id.xperience_ids")
    school_curriculum_ids = fields.One2many('res.partner.school_curriculum', related='partner_id.school_curriculum_ids')
    lang_ids = fields.One2many('res.partner.lang.level',related='partner_id.lang_ids')
    partner_mobile = fields.Char("Mobile", size=32, compute='_compute_partner_adress', store=True)
    salary_proposed = fields.Float("Proposed Salary", group_operator="avg", help="Salary Proposed by the Organisation", compute = '_compute_salary_department')
    destination = fields.Char(string = "Destination", compute = '_compute_salary_department')
   
                  
    @api.depends('partner_id')
    def _compute_partner_adress(self):
        for applicant in self:
            applicant.personal_address= applicant.partner_id.street
            applicant.country_birth= applicant.partner_id.birth_country.name
            applicant.country_residence= applicant.partner_id.residence_country.name        
            applicant.partner_mobile= applicant.partner_id.mobile         
            applicant.civility= applicant.partner_id.civility_id       
            Logger.error(applicant.partner_id.street)
    
    @api.depends('job_id')
    def _compute_salary_department(self):        
        for applicant in self: 
            applicant.destination = applicant.job_id.country_id.name
            applicant.salary_proposed = applicant.job_id.remuneration
            
            
    def filter_all_applicant(self):
        domain = [('job_id.entreprise.country_id.id', '=', self.env.user.partner_id.country_id.id)]
        tree_id = self.env.ref('hr_recruitment.crm_case_tree_view_job').id
        kanban_id = self.env.ref('hr_recruitment.hr_kanban_view_applicant').id
        pivot_id = self.env.ref('hr_recruitment.crm_case_pivot_view_job').id
        graph_id = self.env.ref('hr_recruitment.crm_case_graph_view_job').id
        calendar_id = self.env.ref('hr_recruitment.hr_applicant_calendar_view').id
        activity_id = self.env.ref('hr_recruitment.hr_applicant_view_activity').id
        form_id = self.env.ref('hr_recruitment.hr_applicant_view_form').id
        return {'type': 'ir.actions.act_window',
                'name': 'Candidatures',
                'res_model': 'hr.applicant',
                'view_mode': 'tree',
                'views': [[tree_id,'tree'],[kanban_id, 'kanban'],[pivot_id,'pivot'],[form_id,'form'],[graph_id,'graph'],[calendar_id,'calendar'],[activity_id,'activity']],
                'domain': domain   
            }      