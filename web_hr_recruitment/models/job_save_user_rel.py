import string
from odoo import models, fields, api
from odoo.http import request
import logging
Logger = logging.getLogger(__name__)
from odoo.exceptions import ValidationError

class JobSaveUserRel(models.Model):
    _name = "job.save.user.rel" 
    _description = "Saved Job"  
    
    check = fields.Boolean("Check", default = False)
    jta_id = fields.Many2one('res.users', string = "JTA")
    job_id = fields.Many2one('hr.job', string = "Emploie")
    
   