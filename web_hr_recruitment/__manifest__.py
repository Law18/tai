# -*- coding: utf-8 -*-
{
    'name': "Enterprise Portal Hr Recruitment",
    'description': """
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Website/Website',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['portal', 'website_hr_recruitment'],
    'license': 'LGPL-3',

    # always loaded
    'data': [
        'views/hr_job_portal_template.xml',
        'views/hr_applicant_portal.xml',
        'views/hr_job_view.xml',
        'views/hr_job_website.xml',
        'views/hr_applicant_tai.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/hr_cron_data.xml',
    ],
    'assets': {
        'web.assets_frontend': [
            'web_hr_recruitment/static/src/js/portal_posts_kanban.js'
        ],
    },

}
