odoo.define('web_hr_recruitment.portal_post', function (require) {
'use strict';

var publicWidget = require('web.public.widget');


publicWidget.registry.PortalPost = publicWidget.Widget.extend({
    selector: '.post-container',
    read_events: {
        'change .o_publish': 'postPublish',
    },

    /**
     * @override
     */
    start: async function () {
        var def = this._super.apply(this, arguments);
        return Promise.all([def]);
    },

    postPublish: function(ev){
        var website_published_input = $(ev.currentTarget).is(':checked')
        var post_id = $(ev.currentTarget).prev().prev().val()

        if(website_published_input){
            $(ev.currentTarget).parent().parent().parent().addClass('bg-success')
        }else {
            $(ev.currentTarget).parent().parent().parent().removeClass('bg-success')
        }

        this._rpc({
            model: 'hr.job',
            method: 'write',
            args: [[parseInt(post_id)], {'website_published': website_published_input}],
        }).then()
    },
});

return publicWidget.registry.PortalPost;

});
