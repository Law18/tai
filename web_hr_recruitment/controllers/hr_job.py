# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import werkzeug
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.exceptions import AccessError
from odoo.http import request
from odoo.http import route


class CustomerPortal(CustomerPortal):

    @route(['/my/posts'], type='http', auth="user", website=True, sitemap=False)
    def my_posts(self, **kw):
        posts = request.env['hr.job'].search([('user_id', '=', request.env.uid)])
        return request.render('web_hr_recruitment.my_posts_kanban', {'posts': posts, 'page_name': 'my_posts'})

    @route(['/my/posts/create'], type='http', auth="user", website=True, sitemap=False)
    def create_post(self, **kw):

        if kw and request.httprequest.method == 'POST':
            try:
                job = request.env['hr.job']
                job.check_access_rights('write')
                job.check_access_rule('write')
                res = job.sudo().create(kw)

            except AccessError:
                raise werkzeug.exceptions.NotFound()

            return request.redirect('/my/posts/%s' % res.id)

        departments = request.env['hr.department'].search([])
        return request.render('web_hr_recruitment.my_post_create', {'departments': departments})

    @route(['/my/posts/<int:post_id>'], type='http', auth="user", website=True, sitemap=False)
    def my_post(self, post_id, **kw):
        post = request.env['hr.job'].search([('id', '=', post_id)], limit=1)

        if not post:
            raise werkzeug.exceptions.NotFound()

        items = kw.items()
        kw.update({k: int(v) for (k, v) in items if k in ['department_id']})

        if kw and request.httprequest.method == 'POST':
            if not kw.get('website_published'):
                kw.update({'website_published': 0})

            try:
                kw.update({'id': post.id})
                post.check_access_rights('write')
                post.check_access_rule('write')
                post.sudo().write(kw)

            except AccessError:
                raise werkzeug.exceptions.NotFound()

        departments = request.env['hr.department'].search([])
        return request.render('web_hr_recruitment.my_post', {
            'my_post': post,
            'departments': departments
        })

    @route('''/jobs/apply/<model("hr.job"):job>''', type='http', auth="user", website=True, sitemap=True)
    def jobs_apply(self, job, **kwargs):
        error = {}
        default = {}

        if 'website_hr_recruitment_error' in request.session:
            error = request.session.pop('website_hr_recruitment_error')
            default = request.session.pop('website_hr_recruitment_default')
        return request.render("web_hr_recruitment.apply", {
            'job': job,
            'error': error,
            'default': default,
        })




