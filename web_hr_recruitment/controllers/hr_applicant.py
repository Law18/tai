# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import werkzeug
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.http import request
from odoo.http import route


class CustomerPortal(CustomerPortal):

    @route(['/my/posts/<int:post_id>/applicants'], type='http', auth="user", website=True, sitemap=False)
    def my_post_applicants(self, post_id, **kw):
        if not post_id:
            raise werkzeug.exceptions.NotFound()

        applicants = request.env['hr.applicant'].search([('job_id', '=', post_id)])
        return request.render('web_hr_recruitment.portal_applicants', {
            'applicants': applicants,
            'page_name': 'applicants',
            'post_id': post_id
        })


