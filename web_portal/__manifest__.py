# -*- coding: utf-8 -*-
{
    'name': "web_custom_portal",
    'description': """
        This module adds required base code for a fully integrated customer portal.
It contains the base controller class and base templates. Business addons
will add their specific templates and controllers to extend the customer
portal.

This module contains most code coming from odoo v10 website_portal. Purpose
of this module is to allow the display of a customer portal without having
a dependency towards website editing and customization capabilities.
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Website/Website',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'web', 'website', 'jta_interprise', 'web_hr_recruitment', 'web_tai_auth_signup'],
    'license': 'LGPL-3',

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/base_portal.xml',
        'views/portal_template.xml',
        'views/agent_portal.xml',
        'data/portal_data.xml',
        'data/mail_template_data.xml'
    ],
    'assets': {
        'web.assets_frontend': [
            'web_portal/static/src/js/account_experience.js',
            'web_portal/static/src/js/account_personal_information.js',
            'web_portal/static/src/js/curriculums_vitae.js',
            'web_portal/static/src/js/modal_form_validation.js',
            'web_portal/static/src/js/custom_tabs.js',
            'web_portal/static/src/js/representative_table.js',
            'web_portal/static/src/js/jtaDraftPagination.js',
            'web_portal/static/src/js/jtaCursusPagination.js',
            'web_portal/static/src/js/jtaExperiencePagination.js',
            'web_portal/static/src/js/jtaPublishedPagination.js',
            'web_portal/static/src/js/jtaArchivedPagination.js',
            'web_portal/static/src/js/jtaCandidaturePagination.js'
        ],
    },

}
