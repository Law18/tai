# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import io
from odoo import tools, _
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.http import request
from odoo import http
from odoo.http import route
from itertools import zip_longest
from datetime import datetime
import base64
import logging
Logger = logging.getLogger(__name__)


class CustomerPortal(CustomerPortal):

    PARTNER_FIELDS = ["name", "phone", "email", "street", "city", "country_id", "civility_id", "state_id"]

    def compose_data(self):
        """ Recupération des valeurs/input (multiple) et les transformer au format
        requis pour les enregistrements de type de One2Many ou Many2Many

        format: create => [(0, 0, {})], update => [(1, id, {})] ou unlink => [(2, id)]
        """

        form = request.httprequest.form

        x_actual = form.getlist('x_actual')
        x_end_date = form.getlist('x_end_date')
        x_start_date = form.getlist('x_start_date')
        x_company = form.getlist('x_company')
        x_title = form.getlist('x_title')
        x_contract_type = form.getlist('x_contract_type')
        x_id = form.getlist('x_id')
        x_delete = form.getlist('x_delete')

        to_create = [(0, 0, {'company_name': i[0], 'title': i[1], 'start_date': i[2], 'end_date': i[3], 'contract_type': i[5], 'actual': i[6]}) for i in zip_longest(x_company, x_title, x_start_date, x_end_date, x_id, x_contract_type, x_actual) if i[4] == '-1']
        to_update = [(1, int(i[4]), {'company_name': i[0], 'title': i[1], 'start_date': i[2], 'end_date': i[3], 'contract_type': i[5], 'actual': i[6]}) for i in zip_longest(x_company, x_title, x_start_date, x_end_date, x_id, x_contract_type, x_actual) if i[4] != '-1']
        to_delete = [(2, int(i[0])) for i in zip_longest(x_delete) if i[0] != '']

        return {'to_create': to_create, 'to_update': to_update, 'to_delete': to_delete}

    @route(['/testimoney'], type='http', auth='public', website=True)
    def route_ptest(self, **post):
        return request.render("web_portal.redirect_classic_signup", {})

    @route(['/my/account'], type='http', auth='user', website=True)
    def account(self, redirect=None, **post):
        values = {}
        partner = request.env.user.partner_id

        if request.env.user.user_type == 'representative':
            return request.redirect('/my/account/representative')

        values.update({
            'error': {},
            'error_message': [],
        })

        if post and request.httprequest.method == 'POST':
            ufile = post.get('ufile')
            post.pop('ufile', '')

            lang_id = request.httprequest.form.getlist('lang_id')
            level_id = request.httprequest.form.getlist('level_id')
            id = request.httprequest.form.getlist('id')

            langs = {'lang_ids': [(0, 0, {
                'lang_id': int(i[0]),
                'level_id': int(i[1])
            }) for i in zip_longest(lang_id, level_id, id) if int(i[2]) == -1]}


            relation_fields = ['country_id', 'civility_id', 'gender_id', 'birth_country']
            all_fields = ['birthdate', 'birth_city', 'nationality', 'city', 'street', 'phone'] + relation_fields
            page_fields = {'valid_fields': all_fields, 'relation_fields': relation_fields}

            data = self._handle_post(redirect, post, page_fields)
            if 'clear_image' in post:
                values['image_1920'] = False
            elif ufile:
                image = ufile.read()
                data['image_1920'] = base64.b64encode(image)

            partner.sudo().write(data)
            partner.sudo().write(langs)

            if redirect:
                return request.redirect(redirect)
            return request.redirect('/my/account')

        countries = request.env['res.country'].sudo().search([])
        states = request.env['res.country.state'].sudo().search([])
        applicants = request.env['hr.applicant'].sudo().search([('partner_id', '=', request.env.user.partner_id.id)],limit=4,order='create_date desc')
        savedJob = request.env['hr.job'].sudo().search([('favorite_user_ids', '=', request.env.uid)])
        values.update({
            'partner': partner,
            'countries': countries,
            'applicants': applicants, 
            'savedJob': savedJob, 
            'states': states,
            'civilities': request.env['res.partner.title'].sudo().search([]),
            'genders': request.env['res.partner.sex'].sudo().search([]),
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),
            'redirect': redirect,
            'user': request.env.user,
            'levels': request.env['res.lang.level'].sudo().search([]),
            'lang_level': request.env['res.partner.lang.level'].sudo().search(
                [('partner_id', '=', request.env.user.partner_id.id)]),
            'langs': request.env['res.lang'].sudo().search([]),
            'page_name': 'my_details',
        })

        response = request.render("web_portal.portal_my_account", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    def data_form_validate(self, data):
        error = dict()
        error_message = []

        # Validation
        # for field_name in self.PARTNER_FIELDS:
        #     if not data.get(field_name):
        #         error[field_name] = 'missing'

        # email validation
        if data.get('email') and not tools.single_email_re.match(data.get('email')):
            error["email"] = 'error'
            error_message.append(_('Invalid Email! Please enter a valid email address.'))

        # error message for empty required fields
        if [err for err in error.values() if err == 'missing']:
            error_message.append(_('Some required fields are empty.'))

        unknown = [k for k in data if k not in self.PARTNER_FIELDS]
        if unknown:
            error['common'] = 'Unknown field'
            error_message.append("Unknown field '%s'" % ','.join(unknown))

        return error, error_message

    @route(['/my/account/profile-informations/step/3'], type='http', auth='user', website=True)
    def profile_information_step_3(self, redirect=None, **post):
        values = self._prepare_portal_layout_values()
        values.update({
            'error': {},
            'error_message': [],
        })

        self._handle_post(redirect, post)

        values.update({
            'partner': request.env.user.partner_id,
            'redirect': redirect,
            'page_name': 'general_informations',
        })

        return request.render('web_portal.portal_profile_informations_step_3', values)

    def _handle_post(self, redirect, post, valid_fields):
        qvalues = {}

        if valid_fields:
            # Recupérer que les champs avec des valeurs
            qvalues = {k: v for k, v in post.items() if v != '' and k in valid_fields.get('valid_fields')}
            # Cast
            qvalues.update({k: int(v) for k, v in qvalues.items() if k in valid_fields.get('relation_fields')})

        return qvalues

    @route(['/my/account/personal-informations'], type='http', auth='user', website=True)
    def generals_informations(self, redirect=None, **post):
        values = {'page_name': 'my_personal_informations'}
        partner = request.env.user.partner_id
        values.update({
            'error': {},
            'error_message': [],
        })

        if post and request.httprequest.method == 'POST':
            form_data = {}
            step = post.get('next_step')
            post.pop('next_step')

            relation_fields = ['nationality']
            all_fields = ['birthdate', 'birth_city', 'nationality'] + relation_fields
            page_fields = {'valid_fields': all_fields, 'relation_fields': relation_fields}
            data = self._handle_post(redirect, post, page_fields)

            partner.sudo().write(data)

            form = request.httprequest.form
            lang_id = form.getlist('lang_id')
            level_id = form.getlist('level_id')
            id = form.getlist('id')
            delete = form.getlist('delete')

            form_data.update({'to_create': [(0, 0, {
                'lang_id': int(i[0]),
                'level_id': int(i[1])
                }) for i in zip_longest(lang_id, level_id, id) if int(i[2]) == -1]
            })

            form_data.update({'to_update': [(1, int(i[2]), {
                'lang_id': int(i[0]),
                'level_id': int(i[1])
                }) for i in zip_longest(lang_id, level_id, id) if int(i[2]) != -1]
            })

            form_data.update({'to_delete': [(2, int(i[0])) for i in zip_longest(delete) if i[0] != '']})
            partner.sudo().write({'lang_ids': form_data.get('to_create', []) + form_data.get('to_update', []) + form_data.get('to_delete', [], )})

            if redirect:
                return request.redirect(redirect)
            return request.redirect(step or '/my/home')

        countries = request.env['res.country'].sudo().search([])
        values.update({
            'partner': request.env.user.partner_id,
            'countries': countries,
            'nationalities': countries.filtered(lambda x: x.is_umoa == True),
            'civilities': request.env['res.partner.sex'].sudo().search([]),
            'redirect': redirect,
            'levels': request.env['res.lang.level'].sudo().search([]),
            'lang_level': request.env['res.partner.lang.level'].sudo().search([('partner_id', '=', request.env.user.partner_id.id)]),
            'langs': request.env['res.lang'].sudo().search([]),
        })

        return request.render('web_portal.portal_profile_informations_step_2', values)

    @route(['/my/account/curriculum-vitae'], type='http', auth='user', website=True, sitemap=False)
    def cv_uploader(self, **kw):

        if kw and request.httprequest.method == 'POST':
            Attachment = request.env['ir.attachment']
            partner = request.env.user.partner_id
            ufile = kw.get('ufile')

            attachment_id = Attachment.sudo().create({
                'name': 'CV_%s_%s' % (request.env.user.partner_id.name, datetime.now().strftime("%d-%m-%Y")),
                'res_name': ufile.name,
                'type': 'binary',
                'res_model': 'res.partner',
                'res_id': partner.id,
                'datas': base64.encodebytes(ufile.read())
            })

            if partner.curriculum_vitae:
                partner.curriculum_vitae.unlink()

            partner.write({'curriculum_vitae': attachment_id})



        return request.redirect('/my/account')

    # @route(['/my/account/curriculum-vitae'], type='http', auth='user', website=True, sitemap=False)
    # def cv_uploader(self, **kw):
    #     partner_id = request.env.user.partner_id
    #
    #     if request.httprequest.method == 'POST':
    #         self_attachments = request.env['ir.attachment']
    #         self_curriculums = request.env['curriculum.vitae']
    #
    #         w_values, c_values, u_values = {}, {}, {}
    #
    #         # Enregistrements à créer
    #         files = request.httprequest.files.getlist('file')
    #         file_name = request.httprequest.form.getlist('name')
    #         published = request.httprequest.form.getlist('published')
    #
    #         # Enregistrements à mettre à jour
    #         id = request.httprequest.form.getlist('id')
    #         u_published = request.httprequest.form.getlist('u_published')
    #
    #         u_values = request.httprequest.form.getlist('unlink')
    #
    #         """ Element  à supprimer """
    #         if u_values:
    #             partner_id.write({'curriculum_vitae_attachment_ids': [(2, int(value), 0) for value in u_values]})
    #
    #         """ Element  à mettre à jour """
    #         if u_published and id:
    #             w_values.update({'curriculum_vitae_attachment_ids': [(1, int(value[0]), {'published': int(value[1])}) for value in zip_longest(id, u_published)]})
    #             partner_id.write(w_values)
    #
    #         # Mapper la liste des CV uploadé et preparer le format des données
    #         attach_values = [{
    #             'name': file_name[k],
    #             'res_name': file.name,
    #             'type': 'binary',
    #             'res_model': 'res.partner',
    #             'res_id': partner_id,
    #             'datas': base64.encodebytes(file.read())} for k, file in enumerate(files)]
    #
    #         if attach_values:
    #             attachment_ids = self_attachments.sudo().create(attach_values)
    #
    #             for value in zip_longest(attachment_ids.mapped('id'), published):
    #                 c_values.update({'attachment_id': value[0], 'published': int(value[1]), 'partner_id': partner_id.id})
    #
    #             if c_values:
    #                 cirriculum_ids = self_curriculums.sudo().create(c_values)
    #                 partner_id.write({'curriculum_vitae_attachment_ids': [(4, rec.id, 0) for rec in cirriculum_ids]})
    #
    #         elif not w_values and not c_values:
    #             """ Nottages: aucune donnée passer en paramettre pour mise à jour ou pour création """
    #             partner_id.curriculum_vitae_attachment_ids.unlink()
    #
    #     return request.render('web_portal.portal_cv_uploader', {'curriculums_vitae': partner_id.curriculum_vitae_attachment_ids})

    @route()
    def home(self, **kw):
        return request.redirect("/my/account")

    @route(['/my/account/curriculum-experiences'], type='http', auth='user', website=True, sitemap=False)
    def experiences(self, **kw):
        """ Ajout & mise à jour d'une expérience professionnelle """

        values = {}
        partner = request.env.user.partner_id

        if kw and request.httprequest.method == 'POST':
            Experience = request.env['res.partner.xperience']
            id = kw.get('id', False)
            delete = kw.get('delete', False)
            ufile = kw.get('ufile')

            kw.pop('id', False)
            kw.pop('delete', False)
            kw.pop('ufile', '')

            if ufile:
                image = ufile.read()
                kw['image_1920'] = base64.b64encode(image)

            q = {k: int(v) for k, v in kw.items() if k in ['country_id', 'contract_type_id']}
            kw.update({**{'partner_id': partner.id}, **q})

            # Compléter les valeurs manquantes ou mal formées
            if 'actual' not in kw:
                kw.update({'actual': False})
            if kw.get('end_date', False) == '':
                kw.pop('end_date', False)

            if delete:
                partner.write({'xperience_ids': [(2, int(delete))]})
            elif id:
                partner.write({'xperience_ids': [(1, int(id), kw)]})
            else:
                Experience.create(kw)

        countries = request.env['res.country'].search([])
        contracts_type = request.env['contract.type'].search([])
        study_levels = request.env['study.level'].search([])
        my_experiences = request.env['res.partner.xperience'].sudo()\
            .search([('partner_id', '=', partner.id)], order='start_date desc')
        my_school_curriculum = request.env['res.partner.school_curriculum'].sudo()\
            .search([('partner_id', '=', partner.id)])

        values.update({
            'partner': partner,
            'study_levels': study_levels,
            'countries': countries,
            'contracts_type': contracts_type,
            'my_experiences': my_experiences,
            'my_school_curriculums': my_school_curriculum
        })

        return request.render('web_portal.portal_my_account_curriculum_experiences', values)

    @route(['/my/account/shool-curriculum'], type='http', auth='user', website=True, sitemap=False)
    def school_curriculum(self, **kw):
        """ Ajout & mise à jour du cursus scolaire """

        values = {}
        partner = request.env.user.partner_id

        if kw and request.httprequest.method == 'POST':
            SchoolCurriculum = request.env['res.partner.school_curriculum']
            id = kw.get('id', False)
            delete = kw.get('delete', False)

            kw.pop('id', False)
            kw.pop('delete', False)

            q = {k: int(v) for k, v in kw.items() if k in ['country_id', 'contract_type_id']}
            kw.update({**{'partner_id': partner.id}, **q})

            # Compléter les valeurs manquantes ou mal formées
            if 'actual' not in kw:
                kw.update({'actual': False})
            if kw.get('end_date', False) == '':
                kw.pop('end_date', False)

            if delete:
                partner.write({'school_curriculum_ids': [(2, int(delete))]})
            elif id:
                partner.write({'school_curriculum_ids': [(1, int(id), kw)]})
            else:
                SchoolCurriculum.create(kw)

        return request.redirect('/my/account/curriculum-experiences')

    @route(['/attachment/download', ], type='http', auth='user', sitemap=False)
    def download_attachment(self, attachment_id):
        # Check if this is a valid attachment id
        attachment = request.env['ir.attachment'].sudo().search([('id', '=', int(attachment_id))], limit=1)

        if attachment["datas"]:
            data = io.BytesIO(base64.standard_b64decode(attachment["datas"]))
            return http.send_file(data, filename=attachment['name'], mimetype=attachment['mimetype'], as_attachment=True)
        else:
            return request.not_found()
