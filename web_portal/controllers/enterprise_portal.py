# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.http import request
from odoo import _
import logging
from odoo.http import route

_logger = logging.getLogger(__name__)


class CustomerPortal(CustomerPortal):

    @route(['/my/account/representative', ], type='http', auth='user', website=True, sitemap=False)
    def account_representative(self, **kw):
        User = request.env['res.users']
        user_id = request.env.user
        partner = request.env.user.partner_id


        if kw and request.httprequest.method == 'POST':
            try:
                login = kw.get('email')
                name = kw.get('firstname') + kw.get('lastname')
                kw.update({'login': login, 'name': name, 'registration_state': 'accepted', 'sel_groups': 'group_user'})

                # Template de création d'un res.users & son res.partner associé
                User.sudo()._create_user_from_template(kw)

                user_sudo = User.sudo().search(
                    User._get_login_domain(login) + [('active', '=', False)], order=User._get_login_order(),
                    limit=1
                )
                # Ajouter l'utilisateur crée à la bonne société
                user_sudo.partner_id.write({'parent_id': user_id.partner_id.parent_id})
            except (SignupError, AssertionError) as e:
                if User.sudo().search([('active', '=', False), ('login', '=', login), '|', ('active', '=', True), ('login', '=', login)]):
                    kw["error"] = _("Another user is already registered using this email address.")
                else:
                    _logger.error("%s", e)
                    kw['error'] = _("Could not create a new account.")

        civilities = request.env['res.partner.title'].sudo().search([])
        values = {'child_ids': request.env['res.partner'].search([('is_company' ,'=', False)]) , 'civilities': civilities, 'partner': partner}
        return request.render('web_portal.agent_portal', values)

    @route(['/my/account/<model("hr.job"):job_id>/applicants'], type='http', auth='user', website=True, sitemap=False)
    def account_job_applicants(self, job_id=None, **kw):
        stage_ids = request.env['hr.recruitment.stage'].search([])
        return request.render('web_portal.agent_portal_job_applicants', {'job': job_id, 'stage_ids': stage_ids})

    @route(['/my/account/applicant/<model("hr.applicant"):applicant_id>/toggle_stage'], type='http', auth='user', website=True, sitemap=False)
    def account_applicant_next_stage(self, applicant_id=None, sequence=None, **kw):
        if not sequence:
            return request.not_found()

        stage_id = request.env['hr.recruitment.stage'].search([('sequence', '=', sequence)], limit=1)
        applicant_id.update({'stage_id': stage_id.id})
        return self.account_job_applicants(applicant_id.job_id, **kw)

    @route(['/my/account/applicants'], type='http', auth='user', website=True, sitemap=False)
    def account_applicants(self, job_id=None, **kw):
        jobs = request.env['hr.job'].search([('application_ids', '!=', False), ('website_published', '=', True)])
        return request.render('web_portal.agent_portal_applicants', {'jobs': jobs})

    @route(['/my/account/job-offers', ], type='http', auth='user', website=True, sitemap=False)
    def account_job_offers(self, **kw):

        Job = request.env['hr.job']
        jobs = Job.search(['|', ('active', '=', False), ('entreprise.id', '=', request.env.user.partner_id.parent_id.id)], order='name')

        if kw and request.httprequest.method == 'POST':
            Job.create(kw)
            return request.redirect('/my/account/job-offers')

        countries = request.env['res.country'].search([('is_umoa', '=', True)])
        values = {'jobs': jobs, 'countries': countries}

        return request.render('web_portal.agent_portal_job_offers', values)

    @route(['/my/account/job-offers/<model("hr.job"):job_id>/<string:action>'], type='http', auth='user', website=True, sitemap=False)
    def account_job_offers_actions(self, job_id=None, action=None, **kw):
        values = None

        if not action or not action in ['publish', 'archive']:
            return request.not_found()

        if action == 'archive':
            values = {'active': False}

        job_id.write(values or {'website_published': True})
        return request.redirect('/my/account/job-offers')