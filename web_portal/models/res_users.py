# -*- coding: utf-8 -*-

import logging
from odoo import models, api, _
from odoo.exceptions import UserError
from odoo.addons.auth_signup.models.res_partner import now

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.model
    def create(self, values):
        if self.has_group('base.group_user'):
            values.update({'user_type': 'representative'})
        elif self.has_group('base.group_portal'):
            values.update({'user_type': 'jta'})

        return super(ResUsers, self).create(values)

    """Enable or disable a `res.users` from agent portal"""
    def toggle_active_user(self, active=None):
        self.ensure_one()
        self.sudo().write({'active': active})

        if active:
            self.agent_action_reset_password()

    def agent_action_reset_password(self):
        """ create signup token for each user, and send their signup url by email """
        if self.env.context.get('install_mode', False):
            return
        if self.filtered(lambda user: not user.active):
            raise UserError(_("You cannot perform this action on an archived user."))

        # prepare reset password signup
        # no time limit for initial invitation, only for reset password
        expiration = now(days=+1)
        self.mapped('partner_id').sudo().signup_prepare(signup_type="reset", expiration=expiration)
        # send email to users with their signup url

        template = self.env.ref('web_portal.tai_set_password_email', raise_if_not_found=False)
        if not template:
            template = self.env.ref('auth_signup.reset_password_email')
        assert template._name == 'mail.template'

        email_values = {
            'email_cc': False,
            'auto_delete': True,
            'recipient_ids': [],
            'partner_ids': [],
            'scheduled_date': False,
        }

        for user in self:
            if not user.email:
                raise UserError(_("Cannot send email: user %s has no email address.", user.name))
            email_values['email_to'] = user.email
            # TDE FIXME: make this template technical (qweb)
            with self.env.cr.savepoint():
                force_send = not (self.env.context.get('import_file', False))
                template.send_mail(user.id, force_send=force_send, raise_exception=True, email_values=email_values)
            _logger.info("Password reset email sent for user <%s> to <%s>", user.login, user.email)

