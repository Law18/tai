# -*- coding: utf-8 -*-

from odoo import models, fields


class ResPartner(models.Model):
    _inherit = 'res.partner'

    curriculum_vitae_attachment_ids = fields.One2many('curriculum.vitae', 'partner_id', string='Curriculums vitae')
    curriculum_vitae = fields.Many2one('ir.attachment', string='Curriculums vitae')


class CurriculumVitae(models.Model):
    _name = 'curriculum.vitae'

    attachment_id = fields.Many2one('ir.attachment', string='Curriculums vitae')
    partner_id = fields.Integer(string='Partner')
    published = fields.Boolean('Active', default=True)


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    curriculum_vitae_id = fields.Many2one('curriculum.vitae', string='Curriculums vitae reference', ondelete='cascade', readonly=True, index=True)



