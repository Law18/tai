# -*- coding: utf-8 -*-

from odoo import models, fields


class LangLevel(models.Model):
    _name = 'res.lang.level'
    _description = 'Language level'

    name = fields.Char('Name')