odoo.define('web_portal.account_experience', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    const _t = core._t;

    publicWidget.registry.AccountExperience = publicWidget.Widget.extend({
        selector: '.experience_app',
        read_events: {

            // Upload file
            'click .o_forum_profile_pic_edit': '_onEditProfilePicClick',
            'change .o_forum_file_upload': '_onFileUploadChange',
            'click .o_forum_profile_pic_clear': '_onProfilePicClearClick',

            'click .tai-nav-link': 'activateTab',

            // Experiences
            'click .o_add_experience': 'addExperience',
            'click .o_edit_experience': 'editExperience',
            'change .hold_position': 'onChangeHoldPosition',

            'change .always_in_formation': 'onChangeAlwaysInFormation',

            'click .o_add_school_curriculum': 'addSchoolCurriculum',
            'click .o_edit_school_curriculum': 'editSchoolCurriculum',
            'click .submit_form': 'onSubmit',
            'click .o_delete': 'remove'

        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        start: async function () {
            var def = this._super.apply(this, arguments);
            return Promise.all([def]);
        },


        activateTab: function(ev) {
            var tabs = $(ev.currentTarget).closest('.tai-tabs')
            var currentIdBody = $(ev.currentTarget).data('tai-target')

            tabs.find('.tai-nav-link').removeClass('tai-active-tab')
            $(ev.currentTarget).addClass('tai-active-tab')

            tabs.find('.body-tab').attr('style','display:none !important')
            tabs.find(`${currentIdBody}`).attr('style','display:flex !important')

        },


        /*-------------------------------------------------------------------------------------*/

        onChangeHoldPosition: function(ev){

            this._checkHoldPosition(ev)
        },

        onChangeAlwaysInFormation: function(ev) {
            this._checkHoldPosition(ev)
        },

        _checkHoldPosition: function(ev=null){
            var elementID = `#${$(ev?.currentTarget).closest('.o_modal_account_experiences').attr('id')}`.replace('#undefined','')

            if(this.$el.find(`${elementID || ''} input[name='actual']`).is(':checked')){
                this.$el.find(`${elementID || ''} input[name='end_date']`).prop("readonly", true)
                this.$el.find(`${elementID || ''} input[name='end_date']`).val(null)
                this.$el.find(`${elementID || ''} input[name='end_date']`).prop("required", false)
            }else {
                 this.$el.find(`${elementID || ''} input[name='end_date']`).prop("readonly", false)
                 this.$el.find(`${elementID || ''} input[name='end_date']`).prop("required", true)
            }

        },

        addExperience: function(ev){
            this.refresh()
            this.$el.find('.modal_dynamic_title').text(_t("Ajouter une expérience"))
            this.$el.find('.o_delete').hide()

            /* Retirer le champ `id` du formulaire, afin que lors de la soumission
                le controller ne prenne pas comme un enregistrement à mettre à jour
            */
            this.$el.find(".o_experience_form input[name='id']").remove()

        },

        /* Vider les données du formulaire */
        refresh: function(data=null){
            var fields = this.$el.find('form input, form select')
            fields.each(function() {
                // Mettre tous les champs à jour sauf le `csrf_token`
                if($(this).attr('name') != 'csrf_token') {
                    var attr_name = $(this).attr('name')

                    if($(this).attr('type') == 'checkbox' && data) {
                        $(this).attr('checked', data.actual ? true : false)
                    }
                    else if($(this).attr('type') != 'checkbox'){
                        $(this).val(data ? data[attr_name] : '')
                    }
                }
            });

            this._checkHoldPosition()
        },

        /*----------------------------------------------------------------------------*/

        addSchoolCurriculum: function(ev){
            var targetID = $(ev.currentTarget).data('target')

            this.refresh()

            this.$el.find(`${targetID} .modal_dynamic_title`).text(_t("Ajouter un cursus"))
            this.$el.find(`${targetID} .o_delete`).hide()

            /* Retirer le champ `id` du formulaire, afin que lors de la soumission
                le controller ne prenne pas comme un enregistrement à mettre à jour
            */
            this.$el.find(".o_experience_form input[name='id']").remove()

        },

        editSchoolCurriculum: function(ev) {
            var id = this.editAction(ev)

            this.findOneSchoolCurriculum(id)
                .then((result) => {this.refresh(result[0])})

            // Afficher le modal
            $('#schoolCurriculum').modal()
        },

        findOneSchoolCurriculum: async function (id){
            return await this._rpc({
                model: 'res.partner.school_curriculum',
                method: 'search_read',
                args: [[['id','=',id]], []],
                // limit: 1
            });
        },

        onSubmit: function(ev) {
            var elementID = $(ev.currentTarget).closest('.o_modal_account_experiences').attr('id')

            $(ev.currentTarget).closest(`#${elementID}`).find("form input[id='validate']").trigger('click')
        },

        remove: function(ev){
            var elementID = $(ev.currentTarget).closest('.o_modal_account_experiences').attr('id')

            $(ev.currentTarget).closest(`#${elementID}`).find("form input[name='id']").attr('name', 'delete')
            $(ev.currentTarget).closest(`#${elementID}`).find("form").submit()
        },

        editAction: function(ev) {
            var ID = $(ev.currentTarget).data('id')
            var targetID = $(ev.currentTarget).data('target')

            this.$el.find(`${targetID} form input[name='csrf_token']`)
                .after(`<input type="hidden" name="id" value="${ID}"/>`)

            this.$el.find(`${targetID} .o_delete`).show()
            this.$el.find(`${targetID} .modal_dynamic_title`).text(_t("Modifier le cursus"))

            return ID
        },

        /*----------------------------------------------------------------------------*/

        editExperience: function(ev){
            /* Recupérer depuis la base de données l expérience en questiion et mettre à jour le
                modals pour edition
            */
            var id = $(ev.currentTarget).data('id')

            this.findOneExperience(id)
                .then((result) => {
                    this.refresh(result[0])
                })
            this.$el.find('.o_experience_form').append(`<input type="hidden" name="id" value="${id}"/>`)
            this.$el.find('.o_delete').show()
            this.$el.find('.modal_dynamic_title').text(_t("Modifier l'expérience"))
            // Afficher le modal
            $('#experienceModal').modal()
        },

        findOneExperience: async function (id){
            return await this._rpc({
                model: 'res.partner.xperience',
                method: 'search_read',
                args: [[['id','=',id]], []],
                // limit: 1
            });
        },

    });

    return publicWidget.registry.AccountExperience;

});
