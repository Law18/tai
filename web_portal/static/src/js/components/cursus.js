odoo.define('web_portal.ExperienceList', function (require) {
    "use strict";

    const { Component, tags } = owl;
    const { xml } = tags;
    const { useService }  = require("@web/core/utils/hooks");

    /**
     *
     * Component that can be used in templates to render the custom checkbox of Odoo.
     *
     * <ExperienceList
     *     value="boolean"
     *     disabled="boolean"
     *     text="'Change the label text'"
     *     t-on-change="_onValueChange"
     *     />
     *
     * @extends Component
     */
    class ExperienceList extends Component {
          static template = xml`<tbody t-foreach="experiences" t-as="xperience">
                <tr>
                    <td><input type="date" class="form-control"/></td>
                    <td><input type="date" class="form-control"/></td>
                    <td><input type="text" class="form-control"/></td>
                    <td>Niveau d'études</td>
                    <td>Diplômes</td>
                    <td>Mention</td>
                    <td><a href="#">Remove</a></td>
                </tr>
           </tbody>
          `;

          get experiences() {
                return this.todos;
          }

         setup(){
            console.log(useService("rpc"))
         }

        constructor() {
            super(...arguments);


            console.log('Construct')
            this.todos = [2];

            this.allData()
        }

        async allData(){
            console.log('Before Call')
            return await this.rpc({
                model: 'res.partner',
                args: [[3]],
                method: 'search'
            });//.then((result) => console.log(result));
        }
    }

//    ExperienceList.template = 'web_portal.ExperienceList';


    function setup() {
        const app = new ExperienceList();
        app.mount(document.querySelector('#app-experience'));
    }

    owl.utils.whenReady(setup())

});
