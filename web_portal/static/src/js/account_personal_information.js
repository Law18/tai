odoo.define('web_portal.account_personal_information', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    const _t = core._t;

    publicWidget.registry.MyAccountFirstStep = publicWidget.Widget.extend({
        selector: '.container-fluid-portal',
        read_events: {
            'change #country_id': 'onChangeCountry',
            'click .next_step': 'next',
            'click .previous_step': 'previous',
            // Etape 3
            'click .o_add_level_language': 'addLanguage',
            'click .o_remove_level_language': 'removeLanguage',
            'click .o_validate_form': 'onSubmit',


            // Upload file
            'click .o_forum_profile_pic_edit': '_onEditProfilePicClick',
            'change .o_forum_file_upload': '_onFileUploadChange',
            'click .o_forum_profile_pic_clear': '_onProfilePicClearClick',
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        /**
         * @override
         */
        start: async function () {
            var def = this._super.apply(this, arguments);
            this._applyCountryCode()
            return Promise.all([def]);
        },

        previous: function(ev){
            var currentStep = parseInt($('.stepper').data('stepCounter'))
            var previousStep = currentStep - 1

            if(currentStep > 1) {
                $('.next_step').show()
                $('.o_validate_form').hide()

                $(`.step-page[data-actived-step="${currentStep}"]`).hide()

                $('.stepper').data('stepCounter', previousStep)

                $(`.rounded-step[data-actived-step="${currentStep}"]`).removeClass('passed-step')
                $(`.title-step[data-actived-step="${currentStep}"]`).removeClass('tai-text-primary')
                $(`.step-page[data-actived-step="${previousStep}"]`).show()
            }

            if(previousStep == 1){
                $('.previous_step').hide()
            }
        },

        next: function(ev){
            var currentStep = parseInt($('.stepper').data('stepCounter'))
            var nextStep = currentStep + 1

            if(currentStep < 3) {
                $(`.step-page[data-actived-step="${currentStep}"]`).hide()

                $('.stepper').data('stepCounter', nextStep)

                $(`.rounded-step[data-actived-step="${nextStep}"]`).addClass('passed-step')
                $(`.title-step[data-actived-step="${nextStep}"]`).addClass('tai-text-primary')
                $(`.step-page[data-actived-step="${nextStep}"]`).show()
                $('.previous_step').show()
            }

            if(nextStep == 3){
                $('.next_step').hide()
                $('.o_validate_form').show()
            }
        },

        onChangeCountry: function(){
            this._applyCountryCode()
        },

        _applyCountryCode: function(){
            this.$el.find('#dial_code').val(this.$el.find('#country_id option:selected').data('country-code')).change();
        },

        /*-----------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------STEP3---------------------------------------------------------------*/
        /*-----------------------------------------------------------------------------------------------------------------*/
        addLanguage: function (ev){
            this.buildLanguageField()
                .then((e)=> {
                    $(e).insertBefore('.o_container_add_level_language')
                })
        },

        removeLanguage: function(ev) {

        },

        _getLanguages: function (){
            return this._rpc({
                model: 'res.lang',
                method: 'search_read',
                args: [[], []],
            })
        },

        _getLevels: function (){
            return this._rpc({
                model: 'res.lang.level',
                method: 'search_read',
                args: [[], []],
            })
        },

        buildLanguageField: async function(){
            this.langs = this.langs || await this._getLanguages()
            this.levels = this.levels || await this._getLevels()

            //Create and append select list
            var langList = document.createElement("select");
            var levelList = document.createElement("select");

            langList.id = "lang_id";
            levelList.id = "level_id";

            //Create and append the options
            for (var i = 0; i < this.langs.length; i++) {
                var option = document.createElement("option");
                option.value = this.langs[i].id;
                option.text = this.langs[i].name;
                langList.appendChild(option);
            }

            //Create and append the options
            for (var i = 0; i < this.levels.length; i++) {
                var option = document.createElement("option");
                option.value = this.levels[i].id;
                option.text = this.levels[i].name;
                levelList.appendChild(option);
            }

            return `
                <div class="row py-2">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="email">${_t("Langues parlées")}</label>
                            <select name="lang_id" class="form-control">
                                ${$(langList).html()}
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">${_t("Niveau")}</label>
                            <select name="level_id" class="form-control">
                                ${$(levelList).html()}
                            </select>
                        </div>
                    </div>
                    <div class="col-2 text-right">
                        <a class="btn tai-bg-primary o_remove_level_language bg-danger"><i class="fa fa-minus"></i></a>
                    </div>
                    <input type="hidden" id="id" name="id" value="-1"/>
                </div>
            `;

            /*<tr>

                <input type="hidden" id="id" name="id" value="-1"/>
                <!--<input type="hidden" id="x_delete" name="x_delete"/>-->
                <td><a href="#" data-toggle="modal" data-target="#modaldecline" id="remove"><span class="fa fa-trash" style="font-size:26px;color:red"/></a></td>
            </tr>*/
        },

        onSubmit: function(ev) {
            this.$el.find("input[name='phone']").val(
                (this.$el.find('#dial_code option:selected').text().replace(/\s/g, '') +' '+ this.$el.find("input[name='phone']").val())
            );

            $(ev.currentTarget).closest('#personalInfoModal').find('form').submit()
        },




        /*--------------------------*/
        //Upload

        /**
         * @private
         * @param {Event} ev
         */
        _onEditProfilePicClick: function (ev) {
            // ev.preventDefault();
            $(ev.currentTarget).closest('form').find('.o_forum_file_upload').trigger('click');
        },

        /**
         * @private
         * @param {Event} ev
         */
        _onFileUploadChange: function (ev) {
            if (!ev.currentTarget.files.length) {
                return;
            }

            var reader = new window.FileReader();
            reader.readAsDataURL(ev.currentTarget.files[0]);

            var self = this
            reader.onload = function (ev) {
                self.$el.find('.o_forum_avatar_img').attr('src', ev.target.result);
                self.$el.find('.fill_file').attr('src', ev.target.result)

                self.$el.find('.o_text_profile_pic_edit').hide()
                self.$el.find('.fill_file').show()
            };
        },

        /**
         * @private
         * @param {Event} ev
         */
        _onProfilePicClearClick: function (ev) {
            var $form = $(ev.currentTarget).closest('form');
            $form.find('.o_forum_avatar_img').attr('src', '/web/static/img/placeholder.png');
            $form.append($('<input/>', {
                name: 'clear_image',
                id: 'forum_clear_image',
                type: 'hidden',
            }));
        },

    });

    return publicWidget.registry.MyAccountFirstStep;

});