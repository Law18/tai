let archivedOfferCurrentPage = 1;
let allArchivedOffers = [];
let archivedOfferPerPage = 3;

function recoverArchivedOffers(containerId, elementClass, archivedOfferPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && archivedOfferPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === archivedOfferPerPage || i === elementNb - 1) {
                allArchivedOffers[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function displayArchivedOffers() {
    for (let i = 0; i < allArchivedOffers.length; i++) {
        for (let j = 0; j < allArchivedOffers[i].length; j++) {
            if (i === archivedOfferCurrentPage - 1) {
                allArchivedOffers[i][j].classList.add("d-flex");
                allArchivedOffers[i][j].classList.remove("d-none");
            }
            else {
                allArchivedOffers[i][j].classList.remove("d-flex");
                allArchivedOffers[i][j].classList.add("d-none");
            }
        }
    }
}

function displayArchivedNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = archivedOfferCurrentPage + " / " + allArchivedOffers.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && archivedOfferCurrentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && archivedOfferCurrentPage === allArchivedOffers.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

function nextArchived(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (archivedOfferCurrentPage < allArchivedOffers.length) {
            archivedOfferCurrentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (archivedOfferCurrentPage === allArchivedOffers.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        displayArchivedNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayArchivedOffers();
}

function previousArchived(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI PREVIOUS");
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (archivedOfferCurrentPage > 1) {
            archivedOfferCurrentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (archivedOfferCurrentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        displayArchivedNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayArchivedOffers();
}


recoverArchivedOffers("#_archived", ".archived-jta-mission", archivedOfferPerPage);
displayArchivedNavigation("#archived-offer-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#archived-offer-pagination>.precedent") && document.querySelector("#archived-offer-pagination>.suivant")) {
    document.querySelector("#archived-offer-pagination>.precedent").addEventListener("click", () => {
        previousArchived("#archived-offer-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#archived-offer-pagination>.suivant").addEventListener("click", () => {
        nextArchived("#archived-offer-pagination", ".page-number", ".suivant", ".precedent")
    })
}


displayArchivedOffers();