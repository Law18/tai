odoo.define('web_portal.portal_profile', function (require) {
'use strict';

var publicWidget = require('web.public.widget');
var core = require('web.core');
var QWeb = core.qweb;

publicWidget.registry.ProfilExperience = publicWidget.Widget.extend({
//    xmlDependencies: ['/web_portal/static/src/xml/experience-widget.xml'],
    selector: '#temp',
    read_events: {
        'click #remove': '_remove',

        'click #add': '_add',
        'click #add_school_curriculum': '_add_school_curriculum',

        'click #confirm': '_xConfirmDeleting',
        'click #c-confirm': '_cConfirmDeleting',

        'change #x_actual': '_always',
        'change #c_actual': '_calways',
    },

    /**
     * @constructor
     */
    init: function () {
        this._super.apply(this, arguments);

        this.removing = null;
        this._setDataTable()

    },

    _setDataTable: function(){
        const items = $("#tbody tr");
        const cItems = $("#tbody-my_school-curriculum tr");

        for(let i = 0; i < cItems.length; i++) {
            this._ccheck($(cItems[i]).find('#c_actual'))
        }

        for(let i = 0; i < items.length; i++) {
            this._check($(items[i]).find('#x_actual'))
        }
    },

    _check: function(currentTarget) {
        if(currentTarget.is(':checked')){
            currentTarget.parent().parent().find('#x_end_date').prop("disabled", true);
        }else {
            currentTarget.parent().parent().find('#x_end_date').prop("disabled", false);
        }
    },

    _ccheck: function(currentTarget) {
        if(currentTarget.is(':checked')){
            currentTarget.parent().parent().find('#c_end_date').prop("disabled", true);
        }else {
            currentTarget.parent().parent().find('#c_end_date').prop("disabled", false);
        }
    },

    /**
     * @override
     */
    start: async function () {
        var def = this._super.apply(this, arguments);
        this.levels = await this.getStudyLevels()

        return Promise.all([def]);
    },

    _xConfirmDeleting: function(ev){
        const parent = this.removing.parent().parent()
        // L'élément à supprimer
        const id = parent.find('#x_id')

        // Ajouter un input avec comme valeur l'ID à supprimer, afin de fournir au Controler les items à supprimer de la base de données
        parent.parent().append(`<input type="hidden" id="x_delete" name="x_delete" value=${id.val()} />`)
        parent.remove()
    },

    _cConfirmDeleting: function(ev){
        const parent = this.removing.parent().parent()
        // L'élément à supprimer
        const id = parent.find('#c_id')

        // Ajouter un input avec comme valeur l'ID à supprimer, afin de fournir au Controler les items à supprimer de la base de données
        parent.parent().append(`<input type="hidden" id="c_delete" name="c_delete" value=${id.val()} />`)
        parent.remove()
    },


    _always: function(ev){
        this._check($(ev.currentTarget))
    },
     _calways: function(ev){
        this._ccheck($(ev.currentTarget))
    },

    _buildOption: function(){
        //Create and append select list
        var langList = document.createElement("select");

        langList.id = "lang_id";

        //Create and append the options
        for (var i = 0; i < this.levels.length; i++) {
            var option = document.createElement("option");
            option.value = this.levels[i].id;
            option.text = this.levels[i].name;
            langList.appendChild(option);
        }

        return $(langList).html()

    },

    _add_school_curriculum: function (ev){
        this.$el.find('#tbody-my_school-curriculum').append(`<tr>
                <td><input type="date" name="c_start_date" id="c_start_date" class="form-control" required="1"/></td>
                <td><input type="date" name="c_end_date" id="c_end_date" class="form-control" required="1"/></td>
                <td><input type="checkbox" name="c_actual" id="c_actual"/></td>
                <td><input type="text" name="c_establishment" id="c_establishment" class="form-control" required="1"/></td>

                <td>
                    <select name="c_contract_type" id="c_contract_type" class="form-control">
                        <option value="" disabled="1">Niveau d\'étude...</option>
                        ${this._buildOption()}
                    </select>
                </td>
                <td><input type="text" name="c_diploma" id="c_diploma" class="form-control" required="1"/></td>
                <td><input type="text" name="c_mention" id="c_mention" class="form-control" required="1"/></td>
                <input type="hidden" id="c_id" name="c_id" value="-1"/>/* valeur = -1 signifi qu'il faut créer un nouvelle enregistrement, dans le cas contraire la mettre à jour, ou supprimer */
                <input type="hidden" id="c_delete" name="c_delete"/>
                <td><a href="#" data-toggle="modal" data-target="#modaldecline" t-att-data-id="item.id" id="remove"><span class="fa fa-trash" style="font-size:26px;color:red"/></a></td>
        </tr>`);
    },

    _add: function (ev){
        this.$el.find('#tbody').append(`<tr>
                <td><input type="date" name="x_start_date" id="x_start_date" class="form-control" required="1"/></td>
                <td><input type="date" name="x_end_date" id="x_end_date" class="form-control" required="1"/></td>
                <td><input type="checkbox" name="x_actual" id="x_actual"/></td>
                <td><input type="text" name="x_company" id="x_company" class="form-control" required="1"/></td>
                <td><input type="text" name="x_title" id="x_title" class="form-control" required="1"/></td>
                <td>
                    <select name="x_contract_type" id="x_contract_type" class="form-control">
                        <option value="cdd">CDD</option>
                        <option value="cdi">CDI</option>
                        <option value="internship">Stage</option>
                    </select>
                </td>
                <input type="hidden" id="x_id" name="x_id" value="-1"/>/* valeur = -1 signifi qu'il faut créer un nouvelle enregistrement, dans le cas contraire la mettre à jour, ou supprimer */
                <input type="hidden" id="x_delete" name="x_delete"/>
                <td><a href="#" data-toggle="modal" data-target="#modaldecline" t-att-data-id="item.id" id="remove"><span class="fa fa-trash" style="font-size:26px;color:red"/></a></td>
        </tr>`);
    },

    _edit: function (){},

    _remove: function (ev){
        this.removing = $(ev.currentTarget)
    },

    getStudyLevels: async function (){
        return await this._rpc({
            model: 'study.level',
            method: 'search_read',
            args: [[], []],
        });
    }

});

return publicWidget.registry.ProfilExperience;

});
