let publishedOfferCurrentPage = 1;
let allPublishedOffers = [];
let publishedOfferPerPage = 3;

function recoverPublishedOffers(containerId, elementClass, publishedOfferPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && publishedOfferPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === publishedOfferPerPage || i === elementNb - 1) {
                allPublishedOffers[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function displayPublishedOffers() {
    for (let i = 0; i < allPublishedOffers.length; i++) {
        for (let j = 0; j < allPublishedOffers[i].length; j++) {
            if (i === publishedOfferCurrentPage - 1) {
                allPublishedOffers[i][j].classList.add("d-flex");
                allPublishedOffers[i][j].classList.remove("d-none");
            }
            else {
                allPublishedOffers[i][j].classList.remove("d-flex");
                allPublishedOffers[i][j].classList.add("d-none");
            }
        }
    }
}

function displayPublishedNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = publishedOfferCurrentPage + " / " + allPublishedOffers.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && publishedOfferCurrentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && publishedOfferCurrentPage === allPublishedOffers.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

function nextPublished(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (publishedOfferCurrentPage < allPublishedOffers.length) {
            publishedOfferCurrentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (publishedOfferCurrentPage === allPublishedOffers.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        displayPublishedNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayPublishedOffers();
}

function previousPublished(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI PREVIOUS");
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (publishedOfferCurrentPage > 1) {
            publishedOfferCurrentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (publishedOfferCurrentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        displayPublishedNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayPublishedOffers();
}


recoverPublishedOffers("#_published", ".published-jta-mission", publishedOfferPerPage);
displayPublishedNavigation("#published-offer-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#published-offer-pagination>.precedent") && document.querySelector("#published-offer-pagination>.suivant")) {
    document.querySelector("#published-offer-pagination>.precedent").addEventListener("click", () => {
        previousPublished("#published-offer-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#published-offer-pagination>.suivant").addEventListener("click", () => {
        nextPublished("#published-offer-pagination", ".page-number", ".suivant", ".precedent")
    })
}


displayPublishedOffers();