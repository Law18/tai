let experienceCurrentPage = 1;
let allExperienceOffers = [];
let experienceOfferPerPage = 3;

function recoverExperienceOffers(containerId, elementClass, experienceOfferPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && experienceOfferPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === experienceOfferPerPage || i === elementNb - 1) {
                allExperienceOffers[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function displayExperienceOffers() {
    for (let i = 0; i < allExperienceOffers.length; i++) {
        for (let j = 0; j < allExperienceOffers[i].length; j++) {
            if (i === experienceCurrentPage - 1) {
                allExperienceOffers[i][j].classList.add("d-flex");
                allExperienceOffers[i][j].classList.remove("d-none");
            }
            else {
                allExperienceOffers[i][j].classList.remove("d-flex");
                allExperienceOffers[i][j].classList.add("d-none");
            }
        }
    }
}

function displayExperienceNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = experienceCurrentPage + " / " + allExperienceOffers.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && experienceCurrentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && experienceCurrentPage === allExperienceOffers.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

function nextExperience(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (experienceCurrentPage < allExperienceOffers.length) {
            experienceCurrentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (experienceCurrentPage === allExperienceOffers.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        displayExperienceNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayExperienceOffers();
}

function previousExperience(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI PREVIOUS");
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (experienceCurrentPage > 1) {
            experienceCurrentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (experienceCurrentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        displayExperienceNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayExperienceOffers();
}


recoverExperienceOffers("#_experience", ".experience-mission", experienceOfferPerPage);
displayExperienceNavigation("#experience-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#experience-pagination>.precedent") && document.querySelector("#experience-pagination>.suivant")) {
    document.querySelector("#experience-pagination>.precedent").addEventListener("click", () => {
        previousExperience("#experience-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#experience-pagination>.suivant").addEventListener("click", () => {
        nextExperience("#experience-pagination", ".page-number", ".suivant", ".precedent")
    })
}


displayExperienceOffers();