let draftOfferCurrentPage = 1;
let allDraftOffers = [];
let draftOfferPerPage = 3;

function recoverDraftOffers(containerId, elementClass, draftOfferPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && draftOfferPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === draftOfferPerPage || i === elementNb - 1) {
                allDraftOffers[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function displayDraftOffers() {
    for (let i = 0; i < allDraftOffers.length; i++) {
        for (let j = 0; j < allDraftOffers[i].length; j++) {
            if (i === draftOfferCurrentPage - 1) {
                allDraftOffers[i][j].classList.add("d-flex");
                allDraftOffers[i][j].classList.remove("d-none");
            }
            else {
                allDraftOffers[i][j].classList.remove("d-flex");
                allDraftOffers[i][j].classList.add("d-none");
            }
        }
    }
}

function displayDraftNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = draftOfferCurrentPage + " / " + allDraftOffers.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && draftOfferCurrentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && draftOfferCurrentPage === allDraftOffers.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

function nextDrafts(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (draftOfferCurrentPage < allDraftOffers.length) {
            draftOfferCurrentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (draftOfferCurrentPage === allDraftOffers.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        displayDraftNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayDraftOffers();
}

function previousDrafts(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI PREVIOUS");
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (draftOfferCurrentPage > 1) {
            draftOfferCurrentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (draftOfferCurrentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        displayDraftNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayDraftOffers();
}


recoverDraftOffers("#_draft", ".draft-jta-mission", draftOfferPerPage);
displayDraftNavigation("#draft-offer-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#draft-offer-pagination>.precedent") && document.querySelector("#draft-offer-pagination>.suivant")) {
    document.querySelector("#draft-offer-pagination>.precedent").addEventListener("click", () => {
        previousDrafts("#draft-offer-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#draft-offer-pagination>.suivant").addEventListener("click", () => {
        nextDrafts("#draft-offer-pagination", ".page-number", ".suivant", ".precedent")
    })
}


displayDraftOffers();