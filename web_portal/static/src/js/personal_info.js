odoo.define('web_portal.portal_personal_informations', function (require) {
'use strict';

var publicWidget = require('web.public.widget');
var core = require('web.core');
var QWeb = core.qweb;

publicWidget.registry.ProfilPersonalInformation = publicWidget.Widget.extend({
//    xmlDependencies: ['/web_portal/static/src/xml/experience-widget.xml'],
    selector: '#tmp-step-2',
    read_events: {
        'click #remove': '_remove',
        'click #add': '_add',
        'click #confirm': '_confirmDeleting',
        'change #x_actual': '_always',
    },

    /**
     * @constructor
     */
    init: function () {
        this._super.apply(this, arguments);

        this.removing = null;
    },

    /**
     * @override
     */
    start: async function () {
        var def = this._super.apply(this, arguments);
        this.langs = await this._getLanguages()
        this.levels = await this._getLevels()

        this._cloneTr()

        return Promise.all([def]);
    },

    _cloneTr: function(){
        //Create and append select list
        var langList = document.createElement("select");
        var levelList = document.createElement("select");

        langList.id = "lang_id";
        levelList.id = "level_id";
        //myParent.appendChild(langList);

        //Create and append the options
        for (var i = 0; i < this.langs.length; i++) {
            var option = document.createElement("option");
            option.value = this.langs[i].id;
            option.text = this.langs[i].name;
            langList.appendChild(option);
        }


        //Create and append the options
        for (var i = 0; i < this.levels.length; i++) {
            var option = document.createElement("option");
            option.value = this.levels[i].id;
            option.text = this.levels[i].name;
            levelList.appendChild(option);
        }

        this.item = `<tr>
            <td>
                <select name="lang_id" class="form-control">
                    <option value="" disabled="1">Spoken languages...</option>
                        ${$(langList).html()}
                </select>
            </td>
            <td>
                <select name="level_id" class="form-control">
                    <option value="" disabled="1">Niveau de langue...</option>
                    ${$(levelList).html()}
                </select>
            </td>
            <input type="hidden" id="id" name="id" value="-1"/>
            <!--<input type="hidden" id="x_delete" name="x_delete"/>-->
            <td><a href="#" data-toggle="modal" data-target="#modaldecline" id="remove"><span class="fa fa-trash" style="font-size:26px;color:red"/></a></td>
        </tr>`;
    },

    _confirmDeleting: function(ev){
        const parent = this.removing.parent().parent()
        // L'élément à supprimer
        const id = parent.find('#id')

        // Ajouter un input avec comme valeur l'ID à supprimer, afin de fournir au Controler les items à supprimer de la base de données
        parent.parent().append(`<input type="hidden" id="delete" name="delete" value=${id.val()} />`)
        parent.remove()
    },

    _add: function (ev){
        this.$el.find('#tbody').append(this.item);
    },

    _edit: function (){},

    _remove: function (ev){
        this.removing = $(ev.currentTarget)
    },

    _getLanguages: function (){
        return this._rpc({
            model: 'res.lang',
            method: 'search_read',
            args: [[], []],
        })
    },

    _getLevels: function (){
        return this._rpc({
            model: 'res.lang.level',
            method: 'search_read',
            args: [[], []],
        })
    }

});

return publicWidget.registry.ProfilPersonalInformation;

});
