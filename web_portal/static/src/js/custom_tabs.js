odoo.define('web_portal.custom_tabs', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    publicWidget.registry.CustomTabs = publicWidget.Widget.extend({
        selector: '.tai-tabs',
        read_events: {
            'click .tai-nav-link': 'activateTab',
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        start: async function () {
            var def = this._super.apply(this, arguments);
            return Promise.all([def]);
        },

        activateTab: function(ev) {
            var tabs = $(ev.currentTarget).closest('.tai-tabs')
            var currentIdBody = $(ev.currentTarget).data('tai-target')

            tabs.find('.tai-nav-link').removeClass('tai-active-tab')
            $(ev.currentTarget).addClass('tai-active-tab')

            tabs.find('.body-tab').addClass('d-none')
            tabs.find('.body-tab').removeClass('d-flex')

            tabs.find(`${currentIdBody}`).removeClass('d-none')
            tabs.find(`${currentIdBody}`).addClass('d-flex')

        }

    });

    return publicWidget.registry.CustomTabs;

});
