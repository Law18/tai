let cursusOfferCurrentPage = 1;
let allCursusOffers = [];
let cursusOfferPerPage = 3;

function recoverCursusOffers(containerId, elementClass, cursusOfferPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && cursusOfferPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === cursusOfferPerPage || i === elementNb - 1) {
                allCursusOffers[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function displayCursusOffers() {
    for (let i = 0; i < allCursusOffers.length; i++) {
        for (let j = 0; j < allCursusOffers[i].length; j++) {
            if (i === cursusOfferCurrentPage - 1) {
                allCursusOffers[i][j].classList.add("d-flex");
                allCursusOffers[i][j].classList.remove("d-none");
            }
            else {
                allCursusOffers[i][j].classList.remove("d-flex");
                allCursusOffers[i][j].classList.add("d-none");
            }
        }
    }
}

function displayCursusNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = cursusOfferCurrentPage + " / " + allCursusOffers.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && cursusOfferCurrentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && cursusOfferCurrentPage === allCursusOffers.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

function nextCursus(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (cursusOfferCurrentPage < allCursusOffers.length) {
            cursusOfferCurrentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (cursusOfferCurrentPage === allCursusOffers.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        displayCursusNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayCursusOffers();
}

function previousCursus(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI PREVIOUS");
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (cursusOfferCurrentPage > 1) {
            cursusOfferCurrentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (cursusOfferCurrentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        displayCursusNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayCursusOffers();
}


recoverCursusOffers("#_school_curriculum", ".cursus-mission", cursusOfferPerPage);
displayCursusNavigation("#cursus-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#cursus-pagination>.precedent") && document.querySelector("#cursus-pagination>.suivant")) {
    document.querySelector("#cursus-pagination>.precedent").addEventListener("click", () => {
        previousCursus("#cursus-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#cursus-pagination>.suivant").addEventListener("click", () => {
        nextCursus("#cursus-pagination", ".page-number", ".suivant", ".precedent")
    })
}


displayCursusOffers();