odoo.define('web_portal.modal_validation_form', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    publicWidget.registry.ModalValidation = publicWidget.Widget.extend({
        selector: '.o_modal_validation_form',
        read_events: {
            'click .submit_form': 'onSubmit',
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        /**
         * @override
         */
        start: async function () {
            var def = this._super.apply(this, arguments);
            return Promise.all([def]);
        },

        onSubmit: function(ev) {
            $(ev.currentTarget).closest('.o_modal_validation_form').find("form input[id='validate']").trigger('click')
        },

    });

    return publicWidget.registry.ModalValidation;

});