odoo.define('web_portal.representative_table', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    publicWidget.registry.RepresentativeTable = publicWidget.Widget.extend({
        selector: '.table-childs',
        read_events: {
            'click .i_check': 'handlerAction',
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        /**
         * @override
         */
        start: async function () {
            var def = this._super.apply(this, arguments);

            this.isAccepted()
            return Promise.all([def]);
        },

        isAccepted: function(){

            this.$el.find('tr input[name="active"]').each(function(){
                var attrib = jQuery(this);
                var action = attrib.closest('tr').find('input[name="state_id"]')

                if(attrib.val() === 'True' && !action[0].checked) {
                    action.trigger('click')
                }
            })
        },

        handlerAction: function(ev) {
            var $tr = $(ev.currentTarget).closest('tr')
            var checked = $(ev.currentTarget).is(':checked')
            var user_id = $tr.find('input[name="user_id"]').val()

            if(checked) {
                $tr.find('.online').removeClass('bg-danger')
                $tr.find('.online').text('active')
            }else {
                $tr.find('.online').addClass('bg-danger')
                $tr.find('.online').text('inactive')
            }

            this._rpc({
                model: 'res.users',
                method : 'toggle_active_user',
                args: [[parseInt(user_id)], checked],
            }).then()
        }

    });

    return publicWidget.registry.RepresentativeTable;

});