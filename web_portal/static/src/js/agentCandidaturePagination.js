let candidatureOfferCurrentPage = 1;
let allCandidatureOffers = [];
let candidatureOfferPerPage = 3;

function recoverCandidatureOffers(containerId, elementClass, candidatureOfferPerPage) {
    if (document.querySelectorAll(containerId + ">" + elementClass) && candidatureOfferPerPage > 0) {
        elementClass = document.querySelectorAll(elementClass);
        let elementNb = elementClass.length;
        let j = 0;
        let k = 0;
        let tableauIntermediaire = [];
        for (let i = 0; i < elementNb; i++) {
            tableauIntermediaire[k] = elementClass[i];
            k++;
            if (k === candidatureOfferPerPage || i === elementNb - 1) {
                allCandidatureOffers[j] = tableauIntermediaire;
                j++;
                k = 0;
                tableauIntermediaire = [];
            }
        }
    }
}

function displayCandidatureOffers() {
    for (let i = 0; i < allCandidatureOffers.length; i++) {
        for (let j = 0; j < allCandidatureOffers[i].length; j++) {
            if (i === candidatureOfferCurrentPage - 1) {
                allCandidatureOffers[i][j].classList.add("d-flex");
                allCandidatureOffers[i][j].classList.remove("d-none");
            }
            else {
                allCandidatureOffers[i][j].classList.remove("d-flex");
                allCandidatureOffers[i][j].classList.add("d-none");
            }
        }
    }
}

function displayCandidatureNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    if (document.querySelector(navigationId + ">" + pageNumber)) {
        document.querySelector(navigationId + ">" + pageNumber).innerHTML = candidatureOfferCurrentPage + " / " + allCandidatureOffers.length;
    }
    if (document.querySelector(navigationId + ">" + navigationPrecedent) && candidatureOfferCurrentPage === 1) {
        document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
    }
    if (document.querySelector(navigationId + ">" + navigationSuivant) && candidatureOfferCurrentPage === allCandidatureOffers.length) {
        document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
    }
}

function nextCandidature(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI NEXT");
    if (document.querySelector(navigationId + ">" + navigationSuivant)) {
        if (candidatureOfferCurrentPage < allCandidatureOffers.length) {
            candidatureOfferCurrentPage++;
            document.querySelector(navigationId + ">" + navigationPrecedent).disabled = false;
            if (candidatureOfferCurrentPage === allCandidatureOffers.length) {
                document.querySelector(navigationId + ">" + navigationSuivant).disabled = true;
            }
        }
        displayCandidatureNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayCandidatureOffers();
}

function previousCandidature(navigationId, pageNumber, navigationSuivant, navigationPrecedent) {
    console.log("Entrée dans la fonction TAI PREVIOUS");
    if (document.querySelector(navigationId + ">" + navigationPrecedent)) {
        if (candidatureOfferCurrentPage > 1) {
            candidatureOfferCurrentPage--;
            document.querySelector(navigationId + ">" + navigationSuivant).disabled = false;
            if (candidatureOfferCurrentPage === 1) {
                document.querySelector(navigationId + ">" + navigationPrecedent).disabled = true;
            }
        }
        displayCandidatureNavigation(navigationId, pageNumber, navigationSuivant, navigationPrecedent);
    }
    displayCandidatureOffers();
}


recoverCandidatureOffers("#candidature-mission", ".candidature-mission", candidatureOfferPerPage);
displayCandidatureNavigation("#candidature-pagination", ".page-number", ".suivant", ".precedent");

if (document.querySelector("#candidature-pagination>.precedent") && document.querySelector("#candidature-pagination>.suivant")) {
    document.querySelector("#candidature-pagination>.precedent").addEventListener("click", () => {
        previousCandidature("#candidature-pagination", ".page-number", ".suivant", ".precedent")
    })
    document.querySelector("#candidature-pagination>.suivant").addEventListener("click", () => {
        nextCandidature("#candidature-pagination", ".page-number", ".suivant", ".precedent")
    })
}

displayCandidatureOffers();