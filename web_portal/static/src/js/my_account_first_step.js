odoo.define('web_portal.my_account_first_step', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    publicWidget.registry.MyAccountFirstStep = publicWidget.Widget.extend({
        selector: '#my_account',
        read_events: {
            'change #country_id': 'onChangeCountry',
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        /**
         * @override
         */
        start: async function () {
            var def = this._super.apply(this, arguments);
            this._applyCountryCode()
            return Promise.all([def]);
        },

        onChangeCountry: function(){
            this._applyCountryCode()
        },

        _applyCountryCode: function(){
            this.$el.find('#dial_code').val(this.$el.find('#country_id option:selected').data('country-code')).change();
        },

    });

    return publicWidget.registry.MyAccountFirstStep;

});