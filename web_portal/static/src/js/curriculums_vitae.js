odoo.define('web_portal.curriculums_vitae', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    var QWeb = core.qweb;

    publicWidget.registry.CurriculumsVitae = publicWidget.Widget.extend({
        selector: '.o_curriculum',
        read_events: {
            'click .add_o_curriculum': 'add',
            'click .unlink_o_curriculum': 'unlink',
            'change .o_check_published': 'checkbox',

             'click .o_forum_profile_pic_edit': 'onClickUpload',
             'change .o_file_upload': 'onFileUploadChange',
            'click .submit_form': 'onSubmit',
        },

        /**
         * @constructor
         */
        init: function () {
            this._super.apply(this, arguments);
        },

        /**
         * @override
         */
        start: async function () {
            var def = this._super.apply(this, arguments);
            this.checkbox()
            return Promise.all([def]);
        },

        add: function(){
            var $li = `<li class="list-group-item d-flex justify-content-between align-items-center">
                <input type="file" name="file" class="form-control" required="1"/>
                <input class="form-control ml-2" required="1" name="name"/>
                <span class="badge badge-pill">
                    <input class="o_check_published" type="checkbox"/>
                    <input name="published" value="0" type="hidden"/>
                </span>
                <span class="badge badge-pill unlink_o_curriculum"><a href="#" class="btn-lg text-danger fa fa-trash"/></span>
            </li>`

            $('.o_ul').append($li)
            /* Mettre à jour le DOM (input class='o_check_published' ) */
            this.checkbox()
        },

        unlink: function (ev){
            var curriculum_id = $(ev.currentTarget).parent("li").find("input[name='id']").val()

            this.$el.find(".o_ul").append(`<input name="unlink" type="hidden" value="${curriculum_id}"/>`)
            $(ev.currentTarget).parent().remove()

            /* Mettre à jour le DOM (input class='o_check_published' ) lorsque
            la ligne avec la propriété `checked` a été supprimé */
            this.checkbox()
        },

        checkbox: function(ev) {
            var $inputs = this.$el.find(".o_check_published")

            if($inputs.filter(":checked").length === 1){
                $inputs.filter(":not(:checked)").prop('disabled', 1)

                if(ev){
                    // Enregistrements à créer
                    $(ev.currentTarget).next().val(1)
                    $(ev.currentTarget).filter(".u_check_published").prev().val(1)
                    // Enregistrements à mettre à jour
                    $inputs.filter(".u_check_published :not(:checked)").prev().val(0)
                }
            }
            else {
                $inputs.prop('disabled', 0)
                /* input name='published'*/
                if(ev){
                    // Enregistrements à créer
                    $(ev.currentTarget).next().val(0)
                    $(ev.currentTarget).filter(".u_check_published").prev().val(0)
                    // Enregistrements à mettre à jour
                    $inputs.filter(".u_check_published :not(:checked)").prev().val(1)
                }
            }
        },

        /*-------------------------------------------------------------------------------------------------*/

        onSubmit: function(ev) {
            $(ev.currentTarget).closest('.o_curriculum').find("form input[id='validate']").trigger('click')
        },

        /**
         * @private
         * @param {Event} ev
         */
        onClickUpload: function (ev) {
            // ev.preventDefault();
            $(ev.currentTarget).closest('form').find('.o_file_upload').trigger('click');
        },

        onFileUploadChange: function (ev) {
            if (!ev.currentTarget.files.length) {
                return;
            }

            this.$el.find('.o_text_profile_pic_edit').hide()
            this.$el.find('.fill_file').attr('src', '/web_portal/static/src/img/pdf.svg.webp')
            this.$el.find('.fill_file').show()
        },

    });

    return publicWidget.registry.CurriculumsVitae;

});