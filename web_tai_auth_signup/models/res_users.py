import logging

from ast import literal_eval
from odoo import models, _
from odoo import models, fields
from odoo.tools.misc import ustr
from odoo.addons.auth_signup.models.res_partner import SignupError

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    access_verify_token = fields.Char('Verify Account Token')
    registration_state = fields.Selection(string='Status', default='accepted', selection=[
        ('pending', 'Pending'),
        ('accepted', 'Accepted'),
        ('rejected', 'Rejected')])

    def _create_user_from_template(self, values):
        template_user_id = literal_eval(self.env['ir.config_parameter'].sudo().get_param('base.template_portal_user_id', 'False'))
        template_user = self.browse(template_user_id)

        if values.get('sel_groups', '') == 'group_user':
            # Desactiver par defaut
            template_user = self.env.ref("base.default_user")

        # Retirer la clé `sel_groups` de la liste des values à fournir pour créer un utilisateur
        values.pop('sel_groups', '')

        if not template_user.exists():
            raise ValueError(_('Signup: invalid template user'))

        if not values.get('login'):
            raise ValueError(_('Signup: no login given for new user'))
        if not values.get('partner_id') and not values.get('name'):
            raise ValueError(_('Signup: no name or partner given for new user'))

        # create a copy of the template user (attached to a specific partner_id if given)
        values['active'] = True
        try:
            with self.env.cr.savepoint():
                user = template_user.with_context(no_reset_password=True).copy(values)
                # Desactivé l'ulitisateu l'après avoir créé et relié à un contact
                user.write({'active': False, 'registration_state': values.get('registration_state', 'pending')})

                if user.user_type == 'representative':
                    group_e = self.env.ref('jta_interprise.group_res_partner_agent', False)
                    group_e.sudo().write({'users': [(4, user.id)]})

        except Exception as e:
            # copy may failed if asked login is not available.
            raise SignupError(ustr(e))