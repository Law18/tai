odoo.define('web_auth_signup.button_disabled', function (require) {
'use strict';

var publicWidget = require('web.public.widget');


publicWidget.registry.DynamicSignUp = publicWidget.Widget.extend({
    selector: 'main',
    read_events: {
        'click #approve': '_approve',
        'change #agree': '_hasApproved',
    },

    /**
     * @override
     */
    start: async function () {
        var def = this._super.apply(this, arguments);

        // Désactivation du bouton d'inscription par défaut
        $('#signup').prop('disabled', true);

        return Promise.all([def]);
    },

    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------

    /**
     * @private
     * @param {Event} ev
     */
    _approve: function (ev) {
        if(!$('#agree').is(':checked')) {
            $('#oe_signup_form').find('#agree').trigger('click');
        }
    },
    _hasApproved: function(){
        if($('#agree').is(':checked') ){
            $('#signup').prop('disabled', false);
        }else {
            $('#signup').prop('disabled', true);
        }
    }
});

return publicWidget.registry.DynamicSignUp;

});
