odoo.define('web_auth_signup.enterprise_signup', function (require) {
'use strict';

var publicWidget = require('web.public.widget');


publicWidget.registry.EnterpriseSignUp = publicWidget.Widget.extend({
    selector: '#enterprise_signup',
    read_events: {
        'click #next_step': 'nextStep',
        'click #prev_step': 'prevStep',

        'change #company_country': 'onCompanyCountry',
        'change #country': 'onCountry',
        'click #approve': '_approve',
        'change #agree': '_hasApproved',
       

    },

    /**
     * @override
     */
    start: async function () {
        var def = this._super.apply(this, arguments);

        return Promise.all([def]);
    },

    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------
    nextStep: function (ev) {

        var isDirty = false
        /* Rechercher les champs obligatoires de la première étape*/
        var fields = $(ev.currentTarget
            .closest('form'))
            .find('[required]')
            .filter(function(i){
                return ['company_activity_area_id', 'company_name', 'company_address', 'company_email', 'company_city', 'company_country_id','company_enterprise_type_id','creation_date','staff'].includes($(this).attr('name'))})

        /* Vérifier s'ils sont tous remplis */
        fields.each(function() {
             if(!$(this).val()){
                isDirty = true // Champ pas rempli
             }
        });


        if(!isDirty){
            /* Passer à la deuxième étape du formulaire si tout est bien remplis*/
            this.$el.find('#first_step').hide()
            this.$el.find('#last_step').show()

            /* Mettre à jour la barre d'etat de l'étape */
            this.$el.find('#wizard-step10').removeClass('active')
            this.$el.find('#wizard-step40').addClass('active')
        }

    },
    
     /**
     * @private
     * @param {Event} ev
     */
    _approve: function (ev) {
        if(!$('#agree').is(':checked')) {
            $('#form').find('#agree').trigger('click');
        }
    },
    _hasApproved: function(){
        if($('#agree').is(':checked') ){
            $('#signup').prop('disabled', false);
        }else {
            $('#signup').prop('disabled', true);
        }
    },

    onCompanyCountry: function(ev){
        var country_id = $(ev.currentTarget).val()
        var country_code = $(ev.currentTarget).find(":selected").data('country-code')

        this.$el.find('#company_dial_code').val(country_code).change();
        /* Définir par défault le pays du représentant et l'indicatif du pays */
        this.$el.find('#country').val(country_id)
        this.$el.find('#dial_code').val(country_code).change();
    },

    onCountry: function(ev){
        var country_code = $(ev.currentTarget).find(":selected").data('country-code')
        this.$el.find('#dial_code').val(country_code).change();
    },

    prevStep: function () {
        /* Retourner à la première étape du formulaire */
        this.$el.find('#last_step').hide()
        this.$el.find('#first_step').show()

        /* Mettre à jour la barre d'etat de l'étape */
        this.$el.find('#wizard-step40').removeClass('active')
        this.$el.find('#wizard-step10').addClass('active')
    }
});

return publicWidget.registry.EnterpriseSignUp;

});

