# -*- coding: utf-8 -*-
{
    'name': "web_auth_signup",
    'description': """
        Allow users to sign up and reset their  / With new fields
===============================================
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Hidden/Tools',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web', 'website', 'auth_oauth', 'auth_signup', 'jta_interprise'],
    'license': 'LGPL-3',

    # always loaded
    'data': [
        'views/auth_signup_login_template.xml',
        'views/auth_signup_jta_template.xml',
        'views/auth_signup_enterprise_template.xml',
        'views/res_users.xml',
        'data/email_template.xml',
        'data/email_jta_template.xml',
        'views/auth_signup_reset_password.xml',

        
    ],

    'assets': {
        'web.assets_frontend': [
            'web_tai_auth_signup/static/src/css/tai_signin_signup_style.css',
            'web_tai_auth_signup/static/src/js/auth_signup_jta.js',
            'web_tai_auth_signup/static/src/js/auth_signup_enterprise.js'
        ],
    },
}
