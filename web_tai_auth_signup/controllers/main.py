# -*- coding: utf-8 -*-

import logging
import werkzeug
from odoo import http, _
from odoo.http import request
from odoo.exceptions import UserError
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.addons.web.controllers.main import SIGN_UP_REQUEST_PARAMS
_logger = logging.getLogger(__name__)


class WebAuthSignupHome(AuthSignupHome):

    def _create_company(self, user_sudo):
        # Préparation des paramètres d'enregistrements
        qcontext = {k.replace("company_", ""): v for (k, v) in request.params.items() if
                k in ['company_city', 'company_country_id', 'company_email', 'company_name', 'company_street', 'company_phone', 'company_activity_area_id','company_enterprise_type_id','company_creation_date','company_staff']}
        qcontext.update({'is_company': True})

        # Caster les champs de type relation
        qcontext.update({k: int(v) for (k, v) in qcontext.items() if
                k in ['country_id', 'activity_area_id']})

        """ Créer la raison sociale, puis la rattacher à l'utilisateur connecté 
            (connexion via oauth[Google, etc...]) 
        """
        company_id = request.env['res.partner'].sudo().create(qcontext)
        user_sudo.partner_id.sudo().write({'parent_id': company_id})

    @http.route('/enterprise/signup', type='http', auth='public', website=True, sitemap=False)
    def enterprise_signup(self, **post):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if post and request.httprequest.method == 'POST':
            # Représentant deja crée et loggé
            if request.session.uid:
                # Création de la société
                user_sudo = request.env['res.users'].search([('id', '=', request.session.uid)], limit=1)
                self._create_company(user_sudo)

                return request.redirect('/my')

            else:
                try:
                    qcontext['access_verify_token'] = request.csrf_token()
                    self.do_signup(qcontext)

                    qcontext.pop('access_verify_token', '')
                    User = request.env['res.users']
                    user_sudo = User.sudo().search(
                        User._get_login_domain(qcontext.get('login')) + [('active', '=', False)],
                        order=User._get_login_order(), limit=1
                    )
                    # Création de la société
                    self._create_company(user_sudo)

                    pcontext = {k: v for (k, v) in request.params.items() if k in ['name', 'phone', 'function', 'title', 'city', 'country_id']}
                    # Caster les champs de type relation
                    pcontext.update({k: int(v) for (k, v) in pcontext.items() if
                                 k in ['country_id', 'title']})
                    user_sudo.partner_id.write(pcontext)

                    # Email de validation de compte
                    template = request.env.ref('web_tai_auth_signup.mail_template_enterprise_signup_first_step', raise_if_not_found=False)
                    if user_sudo and template:
                        template.sudo().send_mail(user_sudo.id, force_send=True)

                    return request.redirect('/testimoney')

                except UserError as e:
                    qcontext = request.params
                    qcontext['error'] = e.args[0]
                except (SignupError, AssertionError) as e:
                    qcontext = request.params
                    if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                        qcontext["error"] = _("Another user is already registered using this email address.")
                    else:
                        _logger.error("%s", e)
                        qcontext['error'] = e

        activity_areas = request.env['activity.area'].search([])
        enterprise_types= request.env['enterprise.type'].search([])
        countries = request.env['res.country'].search([('is_umoa', '=', True)])
        civilities = request.env['res.partner.title'].search([])
        qcontext.update({
            'activity_areas': activity_areas,
            'enterprise_types': enterprise_types,
            'countries': countries,
            'civilities': civilities
        })

        return request.render('web_tai_auth_signup.enterprise_signup', qcontext)

    @http.route('/web/type-partner-signup', type='http', auth='public', website=True, sitemap=False)
    def web_pre_auth_signup(self, **post):
        if post and request.httprequest.method == 'POST':
            return request.redirect('/web/signup')
        return request.render('web_tai_auth_signup.pre_auth_signup', {})

    @http.route()
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                qcontext['access_verify_token'] = request.csrf_token()
                self.do_signup(qcontext)

                qcontext.pop('access_verify_token', '')
                # Send an account creation confirmation email
                User = request.env['res.users']
                user_sudo = User.sudo().search(
                    User._get_login_domain(qcontext.get('login')) + [('active', '=', False)], order=User._get_login_order(), limit=1
                )
                template = request.env.ref('web_tai_auth_signup.mail_template_jta_signup',
                                           raise_if_not_found=False)
                if user_sudo and template:
                    template.sudo().send_mail(user_sudo.id, force_send=True)

            except UserError as e:
                qcontext['error'] = e.args[0]
            except (SignupError, AssertionError) as e:
                if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                    qcontext["error"] = _("Another user is already registered using this email address.")
                else:
                    _logger.error("%s", e)
                    qcontext['error'] = e
            if not 'error' in qcontext:
                return request.redirect('/testimoney')

        response = request.render('web_tai_auth_signup.signup', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response


    @http.route('/web/reset_password', type='http', auth='public', website=True, sitemap=False)
    def web_auth_reset_password(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token') and not qcontext.get('reset_password_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                if qcontext.get('token'):
                    self.do_signup(qcontext)
                    return self.web_login(*args, **kw)
                else:
                    login = qcontext.get('login')
                    assert login, _("No login provided.")
                    _logger.info(
                        "Password reset attempt for <%s> by user <%s> from %s",
                        login, request.env.user.login, request.httprequest.remote_addr)
                    request.env['res.users'].sudo().reset_password(login)
                    qcontext['message'] = _("An email has been sent with credentials to reset your password")
            except UserError as e:
                qcontext['error'] = e.args[0]
            except SignupError:
                qcontext['error'] = _("Could not reset your password")
                _logger.exception('error when resetting password')
            except Exception as e:
                qcontext['error'] = str(e)

        response = request.render('web_tai_auth_signup.reset_password', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response


    def get_auth_signup_qcontext(self):
        """ Add fields (phone, etc...) in Shared helper """

        SIGN_UP_REQUEST_PARAMS.update({'phone', 'firstname', 'lastname', 'sel_groups', 'access_verify_token'})
        qcontext = super().get_auth_signup_qcontext()

        if request.params.get('firstname') and request.params.get('lastname'):
            qcontext.update({'name': request.params.get('firstname')+" "+request.params.get('lastname')})

        return qcontext

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = self._prepare_signup_values(qcontext)

        """ Remove following properties for the reset password   """
        if values['sel_groups'] == None:
            values.pop('sel_groups', '')
        if values['access_verify_token'] == None:
            values.pop('access_verify_token', '')

        self._signup_with_values(qcontext.get('token'), values)
        request.env.cr.commit()

    def _signup_with_values(self, token, values):
        request.env['res.users'].sudo().signup(values, token)

    def _prepare_signup_values(self, qcontext):
        values = {key: qcontext.get(key) for key in ('login', 'name', 'password', 'sel_groups', 'access_verify_token')}
        if not values:
            raise UserError(_("The form was not properly filled in."))
        if values.get('password') != qcontext.get('confirm_password'):
            raise UserError(_("Passwords do not match; please retype them."))
        supported_lang_codes = [code for code, _ in request.env['res.lang'].get_installed()]
        lang = request.context.get('lang', '')
        if lang in supported_lang_codes:
            values['lang'] = lang
        return values

    @http.route('/web/signup/account_verify/<model("res.users"):user>/<string:access_verify_token>', methods=['GET'], type='http', auth='public')
    def signup_account_verify(self, user=None, access_verify_token=None, **kwargs):
        login = kwargs.get('login', '')

        if user.access_verify_token == access_verify_token:
            login = user.login
            user.sudo().write({'active': True})
        return request.redirect('/web/login?auth_login=%s' % login)
