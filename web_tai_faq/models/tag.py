from odoo import models , fields

class Tag(models.Model):
    _name = 'web_tai_faq.tag'
    
    name = fields.Text(string='Nom', required=True)
    tag_ids=fields.One2many('web_tai_faq.question','tag_id',string='Question',readonly=True)
    