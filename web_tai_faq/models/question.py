from odoo import models, fields , api
from datetime import date

class Question(models.Model):
    _name = 'web_tai_faq.question'
    _inherit = ['mail.thread']

    name = fields.Text(string='Titre de la question',tracking=True)
    faq_sequence = fields.Integer(string=u'Numéro séquence',default=1,required=True)
    rubric_id=fields.Many2one('web_tai_faq.rubric',ondelete='cascade',string='Rubrique de la question',tracking=True)
    faq_answer = fields.Text(string=u'Réponse de la question', required=True,tracking=True)
    user_creation = fields.Char(string=u'Créée par',readonly=True,default=lambda self:self.env.user.name,tracking=True)
    date_post = fields.Date(string=u'Postée le',readonly=True,tracking=True)
    state = fields.Selection(selection=[('draft', 'Brouillon'),('confirmed', 'Validée'),('posted', 'Postée')],string='Etat',default='draft',readonly=True,tracking=True)
    tag_id=fields.Many2one('web_tai_faq.tag',ondelete='cascade',string='Etiquettes',tracking=True)
    active = fields.Boolean('Active', default=True,tracking=True)

    def action_draft(self):
      self.state = 'draft'

    def action_confirm(self):
      self.state = 'confirmed'

    def action_posted(self):
      self.state = 'posted'
      self.date_post = date.today().strftime('%Y-%m-%d')

    def action_modify(self):
      self.state = 'confirmed'
    
    