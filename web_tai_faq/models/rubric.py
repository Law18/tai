from odoo import models , fields

class Rubric(models.Model):
    _name = 'web_tai_faq.rubric'
    
    name = fields.Text(string='Nom', required=True)
    description = fields.Text(string='Description')
