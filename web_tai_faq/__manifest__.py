# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'FAQ',
    'summary': '',
    'depends': ['base','website'],
    'data': [
        'security/ir.model.access.csv',
        'views/faq.xml',
    ],
    'license': 'LGPL-3',
    'installable': True,
}
