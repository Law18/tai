# -*- coding: utf-8 -*-
{
    'name': "TAI OAuth2 Authentication",
    'description': """
Allow users to login through OAuth2 Provider.
===============================================
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Hidden/Tools',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['website', 'auth_oauth'],
    'license': 'LGPL-3',

    # always loaded
    'data': [
    'views/auth_signup_login_template.xml',
    ],

    'assets': {
        'web.assets_frontend': [
            'web_tai_auth_oauth/static/src/css/style.css',
            'web_tai_auth_oauth/static/src/js/setTitle.js'            
        ],
    },
}
