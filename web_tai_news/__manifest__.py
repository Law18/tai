# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Actualités',
    'summary': '',
    'depends': ['base','web','website'],
    'data': [
        'security/ir.model.access.csv',
        'views/article.xml',
    ],
    'license': 'LGPL-3',
    'installable': True,
    'assets': {
        'web.assets_frontend': [
            
        ],
        'web.assets_backend': [
            'web_tai_news/static/src/scss/website_sale_backend.scss',
        ],    
    } 
}
