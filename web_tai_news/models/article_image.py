 # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError

from odoo.addons.website.tools import get_video_embed_code


class ArticleImage(models.Model):
    _name = 'web_tai_news.image'
    _description = "Article Image"
    _inherit = ['image.mixin']
    _order = 'sequence, id'

    name = fields.Char("Nom de l'image", required=True)
    sequence = fields.Integer(default=10, index=True)
    image_1920 = fields.Image(required=True, verify_resolution=False)
    article_id = fields.Many2one('web_tai_news.article', "Article Template", index=True, ondelete='cascade')
    video_url = fields.Char(u'URL vidéo',
                           help='URL of a video for showcasing your article.')
    embed_code = fields.Html(compute="_compute_embed_code", sanitize=False)
    can_image_1024_be_zoomed = fields.Boolean("Can Image 1024 be zoomed", compute='_compute_can_image_1024_be_zoomed', store=True)

    @api.depends('image_1920', 'image_1024')
    def _compute_can_image_1024_be_zoomed(self):
        for image in self:
            image.can_image_1024_be_zoomed = image.image_1920 and tools.is_image_size_above(image.image_1920, image.image_1024)

    
    @api.depends('video_url')
    def _compute_embed_code(self):
        for image in self:
            image.embed_code = get_video_embed_code(image.video_url)

    @api.constrains('video_url')
    def _check_valid_video_url(self):
        for image in self:
            if image.video_url and not image.embed_code:
                raise ValidationError(_("Provided video URL for '%s' is not valid. Please enter a valid video URL.", image.name))