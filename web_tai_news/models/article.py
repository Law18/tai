from odoo import models, fields , api
from datetime import date
from odoo.exceptions import ValidationError
from odoo import tools
import logging
Logger = logging.getLogger(__name__)


class Article(models.Model):
  _name = 'web_tai_news.article'
  _description = 'TAI News Article'
      
  name = fields.Text(string="Titre", required=True,tracking=True)
  subtitle = fields.Text(string="Sous-titre", required=True,tracking=True)
  rubric_id = fields.Many2one('web_tai_faq.rubric',ondelete='cascade',string=u"Rubrique de l'actualité")
  image = fields.Image("Image Actualités", verify_resolution=False)
  content = fields.Html(string=u"Contenu de l'actualité",tracking=True)
  user_creation = fields.Char(string='Auteur',readonly=True,default=lambda self:self.env.user.name)
  date_post = fields.Date(string=u'Publié le',readonly=True,default=lambda self: fields.Date.today())
  article_ids = fields.One2many('web_tai_news.image', 'article_id', string="Médias Supplémentaires pour l'Article", copy=True)
  state = fields.Selection(selection=[('draft', 'Brouillon'),('confirmed', 'Validé'),('posted', 'Posté')],string='Etat',default='draft',readonly=True,tracking=True)
  
  def action_draft(self):
      self.state = 'draft'

  def action_confirm(self):
    self.state = 'confirmed'

  def action_posted(self):
    self.state = 'posted'
    self.date_post = date.today().strftime('%Y-%m-%d')

  def action_modify(self):
    self.state = 'confirmed'
       
  
class TypeRubric(models.Model):
    _inherit = ['web_tai_faq.rubric']
    
    article_ids = fields.One2many('web_tai_news.article','rubric_id','Articles',domain=[('state','=','posted')])
    type_rubric = fields.Selection(selection=[('actuality', 'Actualité'),('faq', 'FAQ')],string="Type de rubrique",default="actuality")

   
    