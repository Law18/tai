#!/usr/bin/env groovy
def COLOR_MAP = [
	'SUCCESS': 'good',
	'FAILURE': 'danger',
]

pipeline {
	// Setting up local variables for the pipeline
	environment {
		// test variable: 0=success, 1=fail; must be string
		doError = '0'
		GIT_COMMIT_MSG = sh(returnStdout: true, script: "git log -1 --pretty=%B").trim()
	}

	agent any
	stages {
		stage('Checkout on tai') {
			steps {
                script {
                    last_stage = env.STAGE_NAME
                }
				ws("$WORKSPACE" + '/tai/') {
					git branch: 'master',
					credentialsId: 'baamtu_ci_user',
					url: 'http://gitlab.baamtu.com/root/tai.git'
				}
			}
		}

		stage('Prepare files for tai building') {
			steps {
                script {
                    last_stage = env.STAGE_NAME
                }
				ws("$WORKSPACE" + '/tai_build/') {
					sh 'cp -rf ../tai .'

					sh 'ls'
					sh 'rm -r tai/.git*'

					sh 'cp -rf tai/Dockerfile .'
					sh 'cp -rf tai/entrypoint.sh .'
					sh 'cp -rf tai/odoo.conf .'
					sh 'cp -rf tai/web_auth_oauth .'
					sh 'cp -rf tai/web_auth_signup .'
					sh 'cp -rf tai/web_portal .'
					sh 'ls'
				}
			}
		}

		stage('Building docker image for tai and pushing it') {
			steps {
                script {
                    last_stage = env.STAGE_NAME
                }
				ws("$WORKSPACE" + '/tai_build/') {
					script {
                        docker.withRegistry('', 'lamine_docker-auth') {
                          def image = docker.build('baamtu/tai')
                          image.push()
                        }
					}
				}
			}
		}

		stage('Error') {
			// when doError is equal to 1, return an error
			when {
				expression { doError == '1' }
			}
			steps {
				echo "Failure :("
				error "Test failed on purpose, doError == str(1)"
			}
		}
		stage('Success') {
			// when doError is equal to 0, just print a simple message
			when {
				expression { doError == '0' }
			}
			steps {
				echo "Success :)"
			}
		}

	}

	// Post-build actions
	post {
		success {
			slackSend channel: 'tai',
			color: COLOR_MAP[currentBuild.currentResult],
			message: "*${currentBuild.currentResult}:* JOB ${env.JOB_NAME} | BUILD N° = ${env.BUILD_NUMBER}\n Plus d'infos: ${env.BUILD_URL} \n Une nouvelle image est disponible pour le projet TAI sur l'environnement MASTER\n Message du commit : ${env.GIT_COMMIT_MSG} \n Lien du commit: https://gitlab.baamtu.com/root/tai/commit/${env.GIT_COMMIT} "
		}
		failure {
			slackSend channel: 'tai',
			color: COLOR_MAP[currentBuild.currentResult],
			message: "*${currentBuild.currentResult}:* JOB ${env.JOB_NAME} | BUILD N° = ${env.BUILD_NUMBER} | Stage : " + last_stage + "\n Plus d'infos: ${env.BUILD_URL} \n Message du commit : ${env.GIT_COMMIT_MSG} \n Lien du commit: https://gitlab.baamtu.com/root/tai/commit/${env.GIT_COMMIT}"
		}
	}
}
