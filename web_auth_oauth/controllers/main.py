# -*- coding: utf-8 -*-

# import pprint
# import json
# import werkzeug.urls
# import werkzeug.utils
# from odoo.http import request
# from odoo import http
# from odoo.addons.auth_oauth.controllers.main import OAuthLogin, OAuthController, fragment_to_query_string


# class WebOAuthController(OAuthController):
#     @http.route('/auth_oauth/signin', type='http', auth='none')
#     def signin(self, **kw):
#         pprint.pprint(json.loads(kw['state']))
#         return self.signin(**kw)


# class WebAOAuthLogin(OAuthLogin):
#     def list_providers(self):
#         try:
#             providers = request.env['auth.oauth.provider'].sudo().search_read([('enabled', '=', True)])
#         except Exception:
#             providers = []
#         for provider in providers:
#             return_url = request.httprequest.url_root + 'auth_oauth/signin'
#             state = self.get_state(provider)
#             params = dict(
#                 response_type='code',
#                 client_id=provider['client_id'],
#                 redirect_uri=return_url,
#                 scope=provider['scope'],
#                 state=json.dumps(state),
#             )
#             provider['auth_link'] = "%s?%s" % (provider['auth_endpoint'], werkzeug.urls.url_encode(params))
#         return providers
